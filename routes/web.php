<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['namespace' => 'Back'], function(){
    Route::get('login'               ,'LoginController@login')->name('login');
    Route::post('auth'               ,'LoginController@auth')->name('auth');

    Route::group(['middleware' => ['web','auth']], function () {
        Route::get('logout'          ,'LoginController@logout')->name('logout');
        Route::get('/'                   ,'HomeController@index'  )->name('app.home');
        Route::resource('users'          ,'UserController'        );
        Route::resource('warehouse_employees','WarehouseEmployee'  );
        Route::resource('categories'     ,'CategoryController'    );
        Route::resource('subcategories'  ,'SubCategoryController' );
        Route::resource('products'       ,'ProductController'     );
        Route::resource('providers'      ,'ProviderController'    );
        //Save
        Route::resource('save'           ,'SaveController'        );

        Route::resource('bills'          ,'BillController'        );
        Route::resource('supply_bills'   ,'SupplyBillController');

        Route::get('bills/print/{id}'                   ,'BillController@print')->name('bills.print');
        Route::get('bills/change/{id}/{type}'           ,'BillController@change')->name('bills.change');

        //Movement
        Route::get('product/move' ,'MoveController@productMove')->name('move.product');
        Route::get('user/move'    ,'MoveController@userMove')->name('move.user');
        Route::get('provider/moce','MoveController@providerMove')->name('move.provider');

        //Pay user
        Route::get('pay/users'             ,'PaymentController@userPay')->name('pay.users');
        Route::get('pay/users/create'      ,'PaymentController@userPayCreate' )->name('pay.users.create');
        Route::post('pay/users/store'      ,'PaymentController@userPayStore'  )->name('pay.users.store' );
        Route::get('pay/users/edit/{id}'   ,'PaymentController@userPayEdit'   )->name('pay.users.edit'  );
        Route::post('pay/users/update/{id}','PaymentController@userPayUpdate' )->name('pay.users.update');
        Route::get('pay/users/delete/{id}' ,'PaymentController@userPayDestroy')->name('pay.users.delete');

        //Pay Provider
        Route::get('pay/providers'             ,'PaymentController@providerPay'       )->name('pay.providers'        );
        Route::get('pay/providers/create'      ,'PaymentController@providerPayCreate' )->name('pay.providers.create' );
        Route::post('pay/providers/sotre'      ,'PaymentController@providerPayStore'  )->name('pay.providers.store'  );
        Route::get('pay/providers/edit/{id}'   ,'PaymentController@providerPayEdit'   )->name('pay.providers.edit'   );
        Route::post('pay/providers/update/{id}','PaymentController@providerPayUpdate' )->name('pay.providers.update' );
        Route::get('pay/providers/delete/{id}' ,'PaymentController@providerPayDestroy')->name('pay.providers.delete' );



        Route::get('user/bill'                ,'UserBillController@viewUserBill')->name('viewUsersBills');
        Route::get('create/users/bill'        ,'BillController@createUsersBills')->name('createUsersBills');
        Route::post('store/users/bill'        ,'BillController@storeUsersBills')->name('storeUsersBills');
        Route::get('get/bill/users'           ,'BillController@getBillUsers');
        Route::get('providers/bill'           ,'BillController@viewProviderBill')->name('viewProvidersBills');
        Route::get('create/providers/bill'    ,'BillController@createProvidersBills')->name('bills.createProvidersBills');
        Route::post('store/providers/bill'    ,'BillController@storeProvidersBills')->name('bills.storeProvidersBills');
        Route::get('fix/bill'                 ,'BillController@viewFixBill')->name('bills.viewFixBills');
        Route::get('create/fix/bill'          ,'BillController@createFixBill')->name('bills.createFixBills');
        Route::post('store/fix/bill'          ,'BillController@storeFixBill')->name('bills.storeFixBill');
        Route::resource('reports'             ,'ReportController'      );
        Route::get('product/get/product/price','ProductController@getPrice');
        Route::get('product/get/products'     ,'ProductController@getProducts');
        Route::get('user/get/total'           ,'PaymentController@total');
        //Short Fall
        Route::get('provider/shortfalls'            ,'ShortFallController@short')->name('provider.short');
        Route::get('provider/shortfalls/{id}'       ,'ShortFallController@providerShort')->name('provider.short.one');


        // ================== Ajax Route ======================
        Route::group(['prefix' => 'ajax'], function(){
            Route::get('get_subcategory/{main_category_id}', 'AjaxController@get_subcategory')->name('get_subcategory');
            Route::get('search_products', 'AjaxController@search_products')->name('search_products');
            Route::post('product/store', 'AjaxController@product_store')->name('products.store');
        });

        // ================== End Ajax Route ==================
    });


});

Route::group(['prefix' => 'warehouse' , 'namespace'=>'Warehouse','as'=>'warehouse.'], function () {
  Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

  //Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
  //Route::post('/register', 'Auth\RegisterController@register');

  Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
});
