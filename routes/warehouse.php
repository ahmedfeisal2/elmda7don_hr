<?php

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('warehouse')->user();

//     //dd($users);

//     return view('warehouse.home');
// })->name('home');
Route::view('home', 'warehouse.home')->name('home');
Route::get('supply_bills', 'SupplyBillController@index')->name('supply_bills.index');
Route::resource('receipt_bills', 'ReceiptBillController');

// ================== Ajax Route ======================
Route::group(['prefix' => 'ajax'], function () {
    Route::get('get_bills/{user_id}', 'AjaxController@get_bills')->name('get_bills');
    Route::get('get_products/{bill_id}', 'AjaxController@get_products')->name('get_products');

});

// ================== End Ajax Route ==================

