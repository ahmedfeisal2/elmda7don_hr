<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->enum('type',['user','provider'])->nullable();
            $table->string('total_money')->nullable();
            $table->string('last_date')->nullable();
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('personal_phone')->nullable();
            $table->string('phone')->nullable();
            $table->string('code')->unique()->nullable();
            $table->string('max_available_money')->nullable();
            $table->string('max_available_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
