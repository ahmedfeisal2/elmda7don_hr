<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();//
            $table->text('desc')->nullable();//
            $table->string('image')->nullable();//
            $table->string('amount')->nullable();//
            $table->string('max_amount')->nullable();//
            $table->string('min_amount')->nullable();//

            $table->integer('category_id')->unsigned()->nullable();//
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('code')->unique()->nullable();
            $table->string('price')->nullable();//
            $table->string('sell_price')->nullable();//
            $table->string('box_pices_amount')->nullable();//
            $table->string('pice_amount')->nullable();
            $table->string('country')->nullable();
            $table->string('model')->nullable();
            $table->string('unit')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
