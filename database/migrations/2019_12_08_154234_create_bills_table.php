<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date')->nullable(); // تاريخ الفاتورة
            $table->string('native_total')->nullable(); // الإجمالي بدون آي إضافات
            $table->string('total')->nullable(); // الإجمال مع الإضافات إذا وجدت
            $table->enum('type',['supply_bill','receipt_bill','offer_bill','sell_bill'])->default('supply_bill'); // نوع الفاتورة => بيع آو شراء
            $table->enum('status',['pending','done'])->default('pending');// الحالة => عرض سعر آو فاتورة و إذن صرف
            $table->string('paid')->nullable();//المدفوع
            $table->string('reset')->nullable();//الباقي
            $table->enum('discount_type',['none','rate','money'])->default('none');//نوع الخصم  {لايوجد  ، نسبة بالمئة، قيمة مادية}
            $table->string('discount')->nullable(); // الخصم
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('createor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
