<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Admin::create([
            'name'              => 'Manager',
            'email'             => 'admin@email.com',
            'email_verified_at' => now(),
            'password'          => bcrypt('123456789'),
            'phone'             => "1002700084",
            'level'             => 1,
        ]);
    }
}
