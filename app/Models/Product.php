<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];



    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function providers()
    {
        return $this->belongsToMany(User::class, 'provider_products', 'product_id', 'provider_id');
    }

    public function getProfitPercentAttribute()
    {
        $profit = $this->sell_price - $this->price;
        $profit_percent = $profit * 100 / $this->price;
        return number_format($profit_percent,2);
    }


    public function bills()
    {
        return $this->belongsToMany(Bill::class, 'bill_products', 'product_id', 'bill_id');
    }

}
