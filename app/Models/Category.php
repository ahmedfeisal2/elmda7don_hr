<?php

namespace App\Models;

use Exception;
use Faker\Provider\File;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
        protected $guarded = ['id'];

        public function subcategories()
        {
            return $this->hasMany(Category::class,'parent_id');
        }


        public function mainCategory()
        {
            return $this->belongsTo(Category::class,'parent_id');
        }

        public function products()
        {
            return $this->hasMany(Product::class);
        }


}
