<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public function items()
    {
      return $this->hasMany(BillProduct::class,'bill_id');
    }

    public function Products()
    {
        return $this->belongsToMany(Product::class, 'bill_products', 'bill_id', 'product_id')->withPivot('q_price','quantity');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'bill_id', 'id');
    }


    public function payments()
    {
        return $this->hasMany(Payment::class, 'bill_id', 'id');
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id');
    }

}
