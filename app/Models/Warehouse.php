<?php

namespace App\Models;

use App\Notifications\WarehouseResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Warehouse extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','level','phone','code','image','address','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new WarehouseResetPassword($token));
    }

    public function getImageUrlAttribute()
    {
        $image = !is_null($this->attributes['image']) ? $this->attributes['image']  : 'img/user_logo.png';
        return asset($image);
    }
}
