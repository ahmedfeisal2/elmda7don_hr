<?php

namespace App\Models;
use Exception;

use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Hash, DB, Image, Auth, File;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guarded = ['id','password_confirmation'];
    protected $hidden  = ['password','remember_token'];

}
