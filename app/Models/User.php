<?php

namespace App\Models;

use Exception;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash, DB, Image, Auth, File;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $guard = 'web';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (bool) $value;
    }

    public function setPasswordAttribute($value)
    {
      if($value){
        $this->attributes['password'] = Hash::make($value);
      }
    }

    public static function updateStatus($data, $model)
    {
        $value = $data['value'];

        // if the value comes with checked that mean we want the reverse value of it;
        return ($value == 'checked') ? $model->update(['status' => 0]) : $model->update(['status' => 1]);
    }

    public static function createUser($userData)
    {
        if (request()->hasFile('image')) $userData['image'] = uploaded($userData['image'], 'user');

        else $userData = array_except($userData, ['image']);

        $filtered = array_except($userData, ['_token', 'password_confirmation']);

        $createdUser = User::updateOrcreate($filtered);

        return $createdUser;
    }

    public static function updateUser($userData, $currentUser)
    {
        if (request()->hasFile('image'))
        {
            File::delete('public/uploaded/users/' . $currentUser->image);
            $userData['image'] = uploaded($userData['image'], 'user');
        }
        else
        {
            $userData = array_except($userData, ['image']);
        }

        $updatedUser = $currentUser->update($userData);

        return $updatedUser;
    }

    public static function deleteUser($id)
    {
        try
        {
            $user = User::find($id);
            $res['type'] = $user->delete() ? 'success' : 'danger';
            $res['mess'] = $user->delete() ? 'تم حذف العضو بنجاح' : 'حدث خطأ ما';
            return back()->with($res['type'], $res['mess']);
        }
        catch(Exception $e)
        {
            return back('danger', $e->getMessage());
        }
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $users   = [];

        $with_null == null ? $users = $users : $users = $users + [''  => ''];

        $with_main == null ? $users = $users : $users = $users + ['0' => 'Main'];

        $usersDB = User::whereNotIn('id',$exceptedIds)->get();

        foreach ($usersDB as $user) { $users[$user->id] = ucwords($user->name); }

        return $users;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'provider_products', 'provider_id', 'product_id');
    }


    public function bills()
    {
        return $this->hasMany(Bill::class, 'user_id', 'id');
    }


    public function payments()
    {
        return $this->hasMany(Payment::class, 'user_id', 'id');
    }


}
