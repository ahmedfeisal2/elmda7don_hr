<?php

namespace App\Http\Requests\Warehouse\ReceiptBill;

use Illuminate\Foundation\Http\FormRequest;

class ReceiptBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'user_id' => 'required|exists:users,id',
            'bill_id' => 'required|exists:bills,id'
        ];
    }
}
