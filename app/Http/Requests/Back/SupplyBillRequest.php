<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class SupplyBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'user_id'=>'required|exists:users,id',
          'date'=>'required|after_or_equal:'.now()->format('Y-m-d'),
          'products'=>'required|array',
          'total'=>'required|numeric',
          'discount'=>'sometimes|numeric',
        ];
    }
}
