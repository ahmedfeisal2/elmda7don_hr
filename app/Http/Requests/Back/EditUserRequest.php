<?php

namespace App\Http\Requests\Back;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'address'               => 'required|string',
            'company_phone'         => 'required|numeric|unique:users,company_phone,'.$this->user->id,
            'personal_phone'        => 'required|numeric|unique:users,personal_phone,'.$this->user->id,
            'phone'                 => 'required|numeric|unique:users,phone,'.$this->user->id,
            'max_available_money'   => 'required|numeric',
            'max_available_date'    => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'name'=>'الإسم مطلوب',
            'address'=> 'العنوان مطلوب',
            'max_available_money'=>'آقصي قيمة مديونية مطلوب',
            'max_available_date'=>' آقصي مده للسداد مطلوب',
            'company_phone'  => 'عفوا يجب ان يكون رقم الهاتف رقما وغير مكرر',
            'personal_phone' => ' عفوا يجب ان يكون رقم الهاتف رقماوغير مكرر',
            'phone'          => 'عفوا يجب ان يكون رقم الهاتف مكون من رقما وغير مكرر',
        ];
    }
}
