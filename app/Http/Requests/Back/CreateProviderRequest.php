<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'address'               => 'required|string',
            'company_phone'         => 'required|numeric|unique:users,company_phone',
            'personal_phone'        => 'required|numeric|unique:users,personal_phone',
            'phone'                 => 'required|numeric|unique:users,phone',
            'max_available_money'   => 'required|numeric',
            'max_available_date'    => 'required|numeric',
        ];
    }
}
