<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $phone = 'required|numeric|unique:warehouses,phone';
        if($this->route('warehouse_employee')){
          $phone = 'required|numeric|unique:warehouses,phone,'.$this->route('warehouse_employee');
        }

        return [
            'name'                  => 'required|string',
            'address'               => 'sometimes|string',
            'phone'                 => $phone,
            'password'              => 'required_without_all:_method|confirmed',
            'image'                 => 'sometimes|image',
            'email'                 => 'required|email|unique:warehouses,email,'.$this->route('warehouse_employee'),
            'type'                  =>'required|in:manger,employee'
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'الإسم ',
            'address'=> 'العنوان ',
            'phone'          => 'عفوا يجب ان يكون رقم الهاتف مكون من رقما وغير مكرر',
        ];
    }
}
