<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>'required',
            'desc'              =>'sometimes',
            'provider_id'       =>'required|array',
            'provider_id.*'     =>'required|exists:users,id',
            'category_id'       =>'required|exists:categories,id',
            'amount'            =>'required',
            'max_amount'        =>'required',
            'min_amount'        =>'required',
            'price'             =>'required',
            'sell_price'        =>'required',
            // 'box_pices_amount'  =>'required',
            // 'pice_amount'       =>'required',
            'country'           =>'required',
            'model'             =>'required',
            'code'              =>'sometimes|unique:products,code'
            // 'unit'              =>'string',
        ];
    }
}
