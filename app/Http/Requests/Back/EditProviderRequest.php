<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return [
            'name'                  => 'required|string',
            'address'               => 'required|string',
            'company_phone'         => 'required|numeric|unique:users,company_phone,'.$this->provider->id,
            'personal_phone'        => 'required|numeric|unique:users,personal_phone,'.$this->provider->id,
            'phone'                 => 'required|numeric|unique:users,phone,'.$this->provider->id,
            'max_available_money'   => 'required|numeric',
            'max_available_date'    => 'required|numeric',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
