<?php
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File as File;

function category(){ return new App\Models\Category;}
function user(){ return new App\Models\User; }
function admin(){ return new App\Models\Admin;}


function str(){	return new \Str; }
function carbon(){ return new \Carbon\Carbon; }

function direction(){ return LaravelLocalization::getCurrentLocaleDirection();}
function floating($right, $left){ return app()->isLocale('ar') ? $right : $left;}
function getPrice($price){ return $price . ' ' .trans('back.reyal');}
function translated($type, $obj){ return trans('back.' . $type . '-done', ['var' => trans('back.t-' . $obj)]);}

function product_translation(){	return new App\Models\ProductTranslation;}
function category_translation(){ return new App\Models\CategoryTranslation;}
function city_translation(){ return new App\Models\CityTranslation;}

function localeUrl($url, $locale = null){ return LaravelLocalization::getLocalizedURL($locale, $url);}
function cities(){ return city()->whereStatus(1)->get();}
function categories(){ return category()->whereStatus(1)->get();}
function check_if_message_or_image($message){ return (explode('message_', $message)[0] == '') ? getImage('messages',$message) : $message;}
function getAllParagraphs(){ return paragraph()->take(3)->whereStatus(1)->orderBy(DB::raw('RAND()'))->get(); }

function str_limit_30($text){ return ucwords(str_limit($text, 30, '...')); }
function isNullable($text){	return (isset($text) || $text != null || $text != '') ? ucwords($text) : trans('back.no-value'); }
function getTime($time){ return carbon()->createFromFormat('H:i', $time)->format('h:i A'); }
function create_rand_numbers($digits = 4){ return rand(pow(10, $digits-1), pow(10, $digits)-1); }
function dateIsToday($model){ return $model->created_at->isToday(); }
function setActive($url) { return Request::is(app()->getLocale().'/'.$url) ? 'active' : ''; }

function getSetting($setting_name)
{
	$setting = setting()->whereStatus(1)->where('key', $setting_name)->first();
	return $setting ? $setting->value : trans('back.no-value');
}

function permission_checker($auth)
{
    $permissions = $auth->role->permissions;

    foreach($permissions as $permission)
    {
        $name = $permission->permissions;

        $currentRoute = request()->route()->getName();

        if($currentRoute != $name) continue;

        return true;
    }
    return false;
}

function userActivity($user, $type = 'create')
{
    switch ($type)
    {
        case 'update':
            $message_log = 'قام العضو ' . $user->name . ' بتحديث حسابه';
            break;

        case 'delete':
            $message_log = 'قام العضو ' . $user->name . 'بإلغاء حسابه';
            break;

        default:
            $message_log = 'قام العضو ' . $user->name . ' بإنشاء حساب لنفسه';
            break;
    }

    activity('user')->performedOn($user)->causedBy($user->id)->withProperties($user)->log($message_log);

    return true;
}

function apiResponse($data, $type, $msg = '', $code = 200)
{
    if($type == 'success')
    {
        $response = ['data' => $data,'key' => 1,'value' => '1','status' => 'ok'];

        $response['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;

        return response()->json($response, $code);
    }

    $response = ['data' => $data,'key' => 0,'value' => '0','status' => 'fails'];

    $response['message'] = ($msg == '') ? trans('api.server_internal_error') : $msg;

    return response()->json($response, 500);
}

function getAllRoutes()
{
    $routes = Route::getRoutes();
    foreach ($routes as $value)
    {
        if($value->getName() !== null)
        {
            dd($value);
        }
    }
}

function app_commission_for_user($user_id)
{
	$user = user()->find($user_id);

	if($user->advertisements)
	{
		$ads_costs = $user->advertisements->map(function($ad){ return $ad->advertisement->price; })->toArray();
	}
	else
	{
		$ads_costs = [0];
	}


	$percent = ((int)getSetting('app_tax') / 100);

	return round((int)array_sum($ads_costs) * $percent);
}

if (!function_exists('is_url'))
{
	function is_url($url)
	{
		$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
	    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
	    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
	    $regex .= "(\:[0-9]{2,5})?"; // Port
	    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
	    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
	    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

	    // `i` flag for case-insensitive
       	return (preg_match("/^$regex$/i", $url)) ? true : false;
	}
}

function uploaded($img, $folder)
{
    // ini_set("gd.jpeg_ignore_warning", 1);
    // $filename =  $model->getTable() . '_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

    // if (!file_exists(public_path('uploaded/'.$model->getTable().'/')))
    //     mkdir(public_path('uploaded/'.$model->getTable().'/'), 777, true);

    // $path = public_path('uploaded/'.$model->getTable().'/');


    // $dim = getimagesize($img);
    // $width = $width ?? $dim[0];
    // $height = $height ?? $dim[1];

    // Image::make($img)->resize($width, $height, function ($cons) {
    //     $cons->aspectRatio();
    //   })->save($dist . $img->hashName());
    //   $image = $img->hashName();

      $fileName =(time()* rand(1, 99)) . '.' . $img->getClientOriginalExtension();

      $dest = public_path($folder);
      $img->move($dest, $fileName);

      return  $folder . '/' . $fileName;
    // return $filename;
}


function getSubcategory($id, $onlyCount = false)
{
	$sub = category()->where('parent_id', $id);
	if($onlyCount){ return $sub->count(); }
	return $sub->first()->name ?? trans('back.no-value');
}

function getUserCountry($user)
{
	$city_id = $user->city->id;
	$city = city()->find($city_id);
	return $city->country;
}

function getAdditionalImages($path, $_images)
{
    $images = [];

    if($_images) {
	    foreach($_images as $image) { $images[$image->id] = getImage($path, $image->image_name); }
    }

    return empty($images) ? asset('public/admin/assets/images/placeholder.jpg') : $images;
}

function getAdvertiser($advertisement)
{
	$user_advertisement = user_advertisement()->where('advertisement_id', $advertisement->id)->first();

	return isset($user_advertisement->user) ? $user_advertisement : null;
}

function getList($list) { return count($list) > 0 ? ['' => trans('back.select-a-value')] + $list : [0 => trans('back.no-value')]; }

function isFavoriteAdvertisement($advertisement)
{
	$check = favorite()->whereAdvertisementId($advertisement->id)->whereUserId(auth()->id())->count();

	return ($check > 0) ? true : false;
}

function setImageSrc($model, $path)
{
	if($model != null){ if($model->image) return getImage($path, $model->image); }
	return '';
}

function getUserIdFromRequest($request, $withModel = false)
{
	$token = getAuthorizationToken($request);
	$tok = DB::table('tokens')->where('jwt', $token)->first();

	$result = isset($tok->user_id) ? $tok->user_id : 0;

	if($withModel) return user()->find($result);

	return $result;
}

function getAuthorizationToken($request, $onlyBearer = false)
{
    $authorization = $request->header('Authorization');

    $bearer = explode(' ', $authorization);

    return ($onlyBearer) ? $bearer[0] : $bearer[1];
}

function getImage($type, $img)
{
	if ($type == 'users')
		return ($img != null) ? url('public/uploaded/users/' . $img) : asset('public/admin/img/avatar10.jpg');

	elseif ($type == 'admins')
		return ($img != null) ? url('public/uploaded/admins/' . $img) : asset('public/admin/img/avatar10.jpg');

	elseif ($type == 'categories')
		return ($img != null) ? url('public/uploaded/categories/' . $img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif ($type == 'advertisements')
		return ($img != null) ? url('public/uploaded/advertisements/' . $img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif ($type == 'packages')
		return ($img != null) ? url('public/uploaded/packages/' . $img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif ($type == 'banners')
		return ($img != null) ? url('public/uploaded/banners/' . $img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif ($type == 'messages')
		return ($img != null) ? url('public/uploaded/messages/' . $img) : asset('public/admin/assets/images/placeholder.jpg');

	else
		return asset('admin/img/avatar10.jpg');
}

function getAdsByCategoryId($main_id, $sub_id)
{
	$ads = advertisement()->where('category_id', $main_id)->where('supcategory_id', $sub_id)->get();
	return $ads;
}

function models($withColors = false)
{
	if ($withColors) {
		return [
			'teal'   => 'admin',
			'blue'   => 'category',
			'orange' => 'country',
			'green'  => 'city',
			'pink'   => 'user',
			'violet' => 'paragraph',
			'grey'   => 'advertisement',
			'indigo' => 'term',
		];
	}
	// those models for CRUD stystem only;
	return [
		'user-tie'     => 'admin',
		'stack2'       => 'category',
		'flag3'        => 'country',
		'city'         => 'city',
		'users'        => 'user',
		'statistics'   => 'paragraph',
		'magazine'     => 'advertisement',
		'paypal'       => 'package',
		'presentation' => 'banner',
		'gear'         => 'setting',
		// 'screen-full' => 'slider',
	];
}

function convert2english($string) {
    $newNumbers = range(0, 9);
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $string =  str_replace($arabic, $newNumbers, $string);
    return $string;
}

function getModelCount($model, $withDeleted = false)
{
	if ($withDeleted) {
		if ($model == 'admin') return admin()->onlyTrashed()->where('is_super_Admin', '!=', 1)->count();

		$mo = "App\\Models\\" . ucwords($model);

		return $mo::onlyTrashed()->count();
	}

	if ($model == 'admin') return admin()->where('is_super_Admin', '!=', 1)->count();

	if($model == 'category') return category()->whereParentId(0)->count();

	$mo = "App\\Models\\" . ucwords($model);

	return $mo::count();
}

function GetDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
	$latFrom = deg2rad($latitudeFrom);
	$lonFrom = deg2rad($longitudeFrom);
	$latTo = deg2rad($latitudeTo);
	$lonTo = deg2rad($longitudeTo);

	$latDelta = $latTo - $latFrom;
	$lonDelta = $lonTo - $lonFrom;

	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	$x = ($angle * $earthRadius) / 1000;
	return (int) $x;
}

function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
{
	foreach (config('sitelangs.locales') as $lang => $name) {
		foreach ($model->translatedAttributes as $attr) {
			$model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr];
		}
	}

	if ($otherAttrs != null) {
		$otherAttrs = array_except($otherAttrs, ['_method', '_token'] + $excepted);
		foreach ($otherAttrs as $key => $value) {
			$model->$key = $value;
		}
	}
}

function getCategoryByType($type = 0)
{
	$_categories = category()->where('parent_id', $type)->get();

	$categories = array();

	foreach ($_categories as $key => $category) { $categories[$category->id] = $category->name; }

	return $categories;
}

function load_translated_attrs($model)
{
	foreach (config('sitelangs.locales') as $lang => $name) {
		$model->$lang = [];
		foreach ($model->translatedAttributes as $attr) $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
	}
}

function pagination($page, $model, $perPage = 10)
{
	$start  = ($page == 1) ? 0 : ($page - 1) * $perPage;

	$paginatedModel = ($page == 0 ? $model : $model->slice($start, $perPage));

	$total = $model->count();

	$count_pages = (int) $total / (int) $perPage;

	$last_page =  $count_pages >= 1 ? (int) $count_pages : 1;

	$data['total'] 	   = $total;
	$data['paginated'] = $paginatedModel;
	$data['last_page'] = $last_page;
	$data['perPage']   = $perPage;

	return $data;
}

function push_notification($sender, $receiver, $message, $fcm_token, $type)
{
	//send notify
	$fcmNotification['notification']['title']       = "test title";
	$fcmNotification['notification']['body']        = "test body";
	$fcmNotification['notification']['sender']      = $sender;
	$fcmNotification['notification']['receiver']    = $receiver;
	$fcmNotification['notification']['message']     = $message;
	$fcmNotification['notification']['type']        = $type;

	$fcmNotification['priority'] 					= "high";

	$fcmNotification['data']['click_action'] 		= "FLUTTER_NOTIFICATION_CLICK";
	$fcmNotification['data']['title'] 		        = "test title";
	$fcmNotification['data']['body'] 		        = "test body";
	$fcmNotification['data']['sender']       		= $sender;
	$fcmNotification['data']['message']    		    = $message;
	$fcmNotification['data']['receiver']    	    = $receiver;
	$fcmNotification['data']['type']  		        = $type;

	$fcmNotification['to']                          = $fcm_token;

	create_curl_notification($fcmNotification);

	return true;
}

function create_curl_notification($fcmNotification = [])
{
	$fcmUrl = 'https://fcm.googleapis.com/fcm/send';

	$fcmNotification['notification']['sound'] = false;

	$key = getSetting('notification_key');

	$headers = ['Authorization: key=' . $key, 'Content-Type: application/json'];

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $fcmUrl);

	curl_setopt($ch, CURLOPT_POST, true);

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

	$result = curl_exec($ch);

	$response = json_decode($result);

	$res = ($response != null ? true : false);

	curl_close($ch);

	return $res;
}

function payTabsPayment($payment, $request)
{
	$url = 'https://www.paytabs.com/apiv2/verify_payment_transaction';

	$headers = ['content-type: multipart/form-data'];

	$data['merchant_email'] = $payment['email'];
	$data['secret_key']     = $payment['key'];
	$data['transaction_id'] = $request->transaction_id;
	$data['order_id']       = $request->order_id;

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$result = curl_exec($ch);

	$response = json_decode($result);

	$_res = [];

	switch ($response->response_code) {
	    case 4001:
	        $msg = 'Missing parameters.';
	        break;

	    case 4002:
	        $msg = 'Invalid Credentials.';
	        break;

	    case 4003:
	        $msg = 'There are no transactions available.';
	        break;

	    case 0404:
	        $msg = 'You don’t have permissions.';
	        break;

	    case 481:
	        $msg = 'This transaction may be suspicious, your bank holds for further confirmation. Payment Provider has rejected this transaction due to suspicious activity; Your bank will reverse the dedicated amount to your card as per their policy.';
	        break;

	    default:
	        $msg = 'Payment is completed Successfully.';
	        $_res = $response;
	        break;
	}

	$res['msg'] = $msg;
	$res['code'] = $response->response_code;
	$res['result'] = $_res;

	curl_close($ch);

	return $res;
}
