<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\SubCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id','!=',null)->get();
        return view('Front.subcategory.index',compact('categories'));
    }

    public function create()
    {
        $categories = Category::where('parent_id',null)->get()->pluck('name','id');

        return view('Front.subcategory.create',compact('categories'));
    }

    public function edit($id)
    {
        $subcategory = Category::findOrFail($id);
        $categories = Category::where('parent_id',null)->get()->pluck('name','id');
        return view('Front.subcategory.edit', compact('subcategory','categories'));
    }

    public function store(SubCategoryRequest $request)
    {
        Category::create($request->validated());
        return redirect(route('subcategories.index'))->withTrue('تم الإضافة بنجاح');
    }

    public function update(SubCategoryRequest $request, Category $subcategory)
    {
        $subcategory->update($request->validated());
        return redirect(route('subcategories.index'))->withTrue('تم الإضافة بنجاح');

    }

    public function destroy(Category $subcategory)
    {
        $subcategory->delete();
        return response()->json(['value' => 1]);

    }
}
