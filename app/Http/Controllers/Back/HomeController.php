<?php


namespace App\Http\Controllers\Back;
use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Order;
use App\Models\Safe;

class HomeController extends controller
{
    function index()
    {
        $this->data['order1'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*6).'%' )->count();
        $this->data['order2'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*5).'%' )->count();
        $this->data['order3'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*4).'%' )->count();
        $this->data['order4'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*3).'%' )->count();
        $this->data['order5'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*2).'%' )->count();
        $this->data['order6'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24).'%' )->count();
        $this->data['order7'] = Bill::where('status','order')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')).'%' )->count();

        $this->data['bill1'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*6).'%' )->count();
        $this->data['bill2'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*5).'%' )->count();
        $this->data['bill3'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*4).'%' )->count();
        $this->data['bill4'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*3).'%' )->count();
        $this->data['bill5'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*2).'%' )->count();
        $this->data['bill6'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24).'%' )->count();
        $this->data['bill7'] = Bill::where('status','bill')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')).'%' )->count();


        $this->data['offer1'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*6).'%' )->count();
        $this->data['offer2'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*5).'%' )->count();
        $this->data['offer3'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*4).'%' )->count();
        $this->data['offer4'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*3).'%' )->count();
        $this->data['offer5'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24*2).'%' )->count();
        $this->data['offer6'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')-60*60*24).'%' )->count();
        $this->data['offer7'] = Bill::where('status','offer')->where("created_at",'like','%'.date('Y-m-d',strtotime('yesterday')).'%' )->count();

        $this->data['incomm_safe']  = Safe::where('type','incomm')->count();
        $this->data['outcomm_safe'] = Safe::where('type','outcomm')->count();

        return view('Front.includes.index',$this->data);
    }
}
