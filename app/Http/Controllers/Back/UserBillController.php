<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Payment;
use App\Models\Bill;
class UserBillController extends Controller
{
    //Users Bills
    public function viewUserBill()
    {
        $bills =  Bill::where('type','sell')->get();
        $users = User::where('type','user')->get()->pluck('name','id');
        return view('Front.bills.user_bills',compact('bills','users'));
    }

    public function createUsersBills()
    {
        $type = 'sell';
        $products = Product::where('amount','!=','0')->get()->pluck('name','id');
        $users = User::where('type','user')->get()->pluck('name','id');
        return view('Front.bills.create',compact('type','products','users'));
    }

    public function storeUsersBills(Request $request)
    {
        return redirect()->route('');
    }

}
