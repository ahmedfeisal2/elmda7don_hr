<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\CreateProviderRequest;
use App\Http\Requests\Back\EditProviderRequest;
use App\Models\Bill;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\User;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = User::where('type','provider')->get();
        return view('Front.providers.index',compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Front.providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProviderRequest $request)
    {
        User::create($request->validated()+["type"=>'provider']);
        return redirect(route('providers.index'))->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = User::findOrFail($id);
        return view('Front.providers.edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProviderRequest $request, User $provider)
    {
        $provider->update($request->validated());
        return redirect(route('providers.index'))->withTrue('تم التعديل بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['value' => 1]);
    }

    public function wantView()
    {
        return view('');
    }


    public function short()
    {
        $products  = Product::where('amount','<','1')->get();
        return view('Front.providers.shortfall_products',compact('products'));
    }


    public function move()
    {
        $user = '';
        $bill  = Bill::where(['type'=>'buy','user_id'=>$user->id])->get();
    }

}
