<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\ProductRequest;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class AjaxController extends Controller
{
  public function get_subcategory($main_category_id)
  {
      $category = Category::findOrFail($main_category_id);
      $categories = $category->subcategories()->pluck('name', 'id');
      $view = view('Front.products.ajax.get_subcategory', compact('categories'))->render();
      return response()->json(['value' => 1, 'view' => $view]);
  }

    public function search_products(Request $request)
    {
        $products = Product::where('code', 'like', '%' . $request->all()['query'] . '%')
            ->orwhere('name', 'like', '%' . $request->all()['query'] . '%')->get(['id', 'code', 'name', 'price']);
        return $products;
    }

    public function product_store(ProductRequest $request)
    {
        $inputs = array_except($request->validated(), ['provider_id']);
        $product = Product::create($inputs);
        $product->providers()->attach($request->provider_id);
        return response()->json(['value' => 1]);
    }
}
