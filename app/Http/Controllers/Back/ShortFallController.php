<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;

class ShortFallController extends Controller
{
    public function short()
    {
        $products  = Product::where('amount','<','1')->get();
        return view('Front.providers.shortfall_products',compact('products'));
    }

    public function providerShort(Request $request)
    {
        $user      = User::find($request->id);
        $products  = Product::where('amount','<','1')->get();
        return view('Front.providers.shortfall_products',compact('products','user'));
    }

}
