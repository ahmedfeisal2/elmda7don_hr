<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\SaveRequest;
use App\Models\Safe;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $safes  = Safe::all();
        return view('Front.safes.index',compact('safes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Front.safes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveRequest $request)
    {

        $last = Safe::get()->last();

        $new_safe = new Safe();
        $new_safe->type = $request->type;
        if($request->type != 'incomm' && $last->money_after == '0'){
            return redirect()->back()->withFalse('الخزنه فارغه');
        }elseif($request->money < 0){
            return redirect()->back()->withFalse('لايمكن إيداع مبلغ بالسالب');
        }
        if($last){
            $new_safe->money_before = $last->money_after;
        }else{
            $new_safe->money_before = 0 ;
        }
        $new_safe->current_money = $request->money;
        if($last){
         if($request->type =='incomm'){
            $new_safe->money_after = $new_safe->money_before + $request->money;
         }else{
            if($new_safe->money_before - $request->money < 0 ){
                return redirect()->back()->withFalse('قيمة الملبغ آكبر من محتوي الخزنه');
            }else{
                $new_safe->money_after = $new_safe->money_before - $request->money;
            }
         }

        }else{
          if($request->type =='incomm'){
            $new_safe->money_after = $request->money;
          }else{
            return redirect()->back()->withFalse('لايمكن سحب  المبلغ الخزنه فارغه');
         }

        }
        $new_safe->date        = date('Y-m-d');
        $new_safe->save();
        return redirect(route('save.index'))->withTrue('تمت الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $save = Safe::find($id);
        return view('Front.safes.edit',compact('save'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveRequest $request, $id)
    {
        $save = Safe::find($id);
        $save->update($request->validated());
        return redirect()->route('save.index')->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $save = Safe::find($id);
        $save->delete();
        return response()->json(['value' => 1]);

    }
}
