<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\CategoryRequest;

use App\Models\Category;
use Exception;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id',null)->get();
        return view('Front.categories.index',compact('categories'));
    }

    public function create()
    {
        return view('Front.categories.create');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('Front.categories.edit', compact('category'));
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->validated());
        return redirect(route('categories.index'))->withTrue('تم الإضافة بنجاح');

    }

    public function update(CategoryRequest $request, Category $category)
    {
             $category->update($request->validated());
            return redirect(route('categories.index'))->withTrue('تم الإضافة بنجاح');

    }

    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json(['value' => 1]);

    }
}
