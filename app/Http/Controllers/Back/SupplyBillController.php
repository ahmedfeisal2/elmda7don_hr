<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bill;
use App\Http\Requests\Back\SupplyBillRequest;

class SupplyBillController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->path = 'Front.bills.supply_bill.';
    }
   public function index()
    {
      $bills = Bill::whereType('supply_bill')->latest()->get();
      return view($this->path.'index',compact('bills'));
    }
    public function create()
    {
        $users = User::where('type', 'provider')->get()->pluck('name', 'id');
        $categories = Category::where('parent_id', null)->get()->pluck('name', 'id');
        $providers = User::where('type', 'provider')->get()->pluck('name', 'id');
        $subcategories = Category::where('parent_id', null)->get()->pluck('name', 'id');
        $latest_bill = Bill::latest()->first();
        if (!is_null($latest_bill)) {
            $code = $latest_bill->id + 1;
        } else {
            $code = 1;
        }
        return view($this->path . 'create', compact('users', 'code', 'categories', 'providers', 'subcategories'));
    }
    public function store(SupplyBillRequest $request)
    {
        \DB::beginTransaction();
          try {
            $inputs = array_except($request->validated(), ['products','discount']);
            $inputs['createor_id'] = auth()->id();
            $inputs['type'] = 'supply_bill';
            $inputs['status'] = 'pending';
            $bill =  Bill::create($inputs);

            $bill->items()->createMany($request->products);
            \DB::commit();
            } catch (\Exception $e) {
              \DB::rollback();
              return back()->withFalse('حدث خطأ أثناء التسجيل الرجاء المحاولة مرة أخري');
            }
            return redirect()->route('supply_bills.index')->withTrue('تم إضافة الفاتورة بنجاح');

    }
}
