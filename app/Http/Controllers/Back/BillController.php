<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\SellBillRequest;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\BillProduct;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Exception;

class BillController extends Controller
{
    public function index($type)
    {
      switch ($type) {
        case 'supply':
          // code...
          break;
      }
        $bills = Bill::get();
        return view('Front.bills.index', compact('bills'));
    }

    public function create(Request $request)
    {
      switch ($request->type) {
        case 'supply':
          $users = User::where('type','provider')->get()->pluck('name','id');
          break;
      }
        // $products = Product::where('amount','!=','0')->get()->pluck('name','id');

        return view('Front.bills.create',compact('users'));
    }

    public function store(SellBillRequest $request)
    {
            $inputs = array_except($request->validated(), ['product','amount','price']);
            $inputs['date'] = Carbon::now()->format('Y-m-d');
            for ($i=0; $i < count($request->product); $i++) {
                $orignalProduct = Product::find($request->product[$i]);
                if($orignalProduct->amount < 1  || $orignalProduct->max_amount < $request->amount[$i]){
                    return redirect()->route('bills.create')->withFalse('لايمكن إتمام الفاتورة بهذه الكمية من منتج '.$orignalProduct->name);
                }
            }

            $bill =  Bill::create($inputs);
            for ($i=0; $i < count($request->product); $i++) {
             $product =    new BillProduct ();
             $product->bill_id    = $bill->id;
             $product->product_id = $request->product[$i];
             $product->quantity   = $request->amount[$i];
             $product->price      = $request->price[$i] / $request->amount[$i];
             $product->q_price    = $request->price[$i];
             if($request->avilable){
                $product->avilable = $request->avilable;
             }else{
               $orignalProduct = Product::find($request->product[$i]);
               if($orignalProduct->amount < 1 ){
                $product->avilable = 0;
               }
             }
             if($request->bill_price){
                $product->bill_price = $request->bill_price[$i];
             }

             $product->save();
            }
            return redirect()->route('bills.index')->withTrue('تم إضافة الفاتورة بنجاح');

    }


    public function edit($id)
    {
        $bill = Bill::findOrFail($id);
        $products = Product::where('amount','!=','0')->get()->pluck('name','id');
        $users = User::get()->pluck('name','id');

        return view('Front.bills.edit', compact('bill','products','users'));
    }


    public function update(SellBillRequest $request,Bill  $bill)
    {
        if($bill->products){
            $bill->products()->detach();
        }


        $inputs = array_except($request->validated(), ['product','amount','price']);


        for ($i=0; $i < count($request->product); $i++) {
            $orignalProduct = Product::find($request->product[$i]);
            if($orignalProduct->amount < 1  || $orignalProduct->max_amount < $request->amount[$i]){
                return redirect()->route('bills.edit',[$bill->id])->withFalse('لايمكن إتمام الفاتورة بهذه الكمية من منتج '.$orignalProduct->name);
            }
        }

        $bill->update($inputs);
        for ($i=0; $i < count($request->product); $i++) {
         $product =    new BillProduct ();
         $product->bill_id    = $bill->id;
         $product->product_id = $request->product[$i];
         $product->quantity   = $request->amount[$i];
         $product->price      = $request->price[$i] / $request->amount[$i];
         $product->q_price    = $request->price[$i];
         if($request->avilable){
            $product->avilable = $request->avilable;
         }else{
           $orignalProduct = Product::find($request->product[$i]);
           if($orignalProduct->amount < 1 ){
            $product->avilable = 0;
           }
         }
         if($request->bill_price){
            $product->bill_price = $request->bill_price[$i];
         }

         $product->save();
        }
        return redirect()->route('bills.index')->withTrue('تم تعديل الفاتورة بنجاح');


    }

    public function destroy(Bill $bill)
    {

        $bill->products()->detach();
        $bill->delete();
        return response()->json(['value' => 1]);
    }

    public function getBillUsers(Request $request)
    {
        if($request->type == 'buy' ){
            $users = User::where('type','provider')->get()->pluck('name','id');
        }else{
            $users = User::where('type','user')->get()->pluck('name','id');
        }
        return  response()->json(['success','users'=>$users]);

    }

    public function print($id)
    {
        $bill = Bill::find($id);
        return view('Front.bills.print',compact('bill'));
    }

    public function change($id,$type)
    {
        $bill = Bill::find($id);
        if($type == 'bill'){
           $bill->status = 'bill';
        }elseif($type=='order'){
            $bill->status = 'order';
            foreach($bill->products as $product){
                $newProduct = Product::find($product->id);
                $newProduct->amount  -= $product->pivot->quantity;
                $newProduct->save();
            }
            $user = User::find($bill->user_id);
            if($user->total_money ==''||$user->total_money == null ||$user->total_money ==' '){
                $user->total_money  = $bill->total;
            }else{
                $user->total_money  += $bill->total;
            }
            $user->last_date = Carbon::now()->format('Y-m-d');
            $user->save();
            $order = new Order();
            $order->bill_id = $bill->id;
            $order->rest    = $bill->total;
            $order->save();
        }
        $bill->save();
       return  redirect()->route('bills.index')->withTrue('تم تعديل الفاتورة بنجاح');
    }



}
