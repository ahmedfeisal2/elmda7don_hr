<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('Front.products.index',compact('products'));
    }

    public function create()
    {
        $categories = Category::where('parent_id',null)->get()->pluck('name','id');
        $providers = User::where('type','provider')->get()->pluck('name','id');
        $subcategories = Category::where('parent_id',null)->get()->pluck('name','id');
        return view('Front.products.create',compact('categories','providers','subcategories'));
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::where('parent_id',null)->get()->pluck('name','id');
        $providers = User::where('type','provider')->get()->pluck('name','id');
        $subcategories = Category::where('parent_id',null)->get()->pluck('name','id');
        return view('Front.products.edit', compact('product','categories','providers','subcategories'));
    }

    public function store(ProductRequest $request)
    {
        $inputs = array_except($request->validated(), ['provider_id']);
        $product = Product::create($inputs);
        $product->providers()->sync($request->provider_id);
        return redirect(route('products.index'))->withTrue('تم الإضافة بنجاح');
    }

    public function update(ProductRequest $request, Product $product)
    {
        $inputs = array_except($request->validated(), ['provider_id']);
        $product->update($inputs);
        if($product->providers){
            $product->providers()->detach();
        }

        $product->providers()->attach($request->provider_id);
        return redirect(route('products.index'))->withTrue('تم الإضافة بنجاح');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(['value' => 1]);
    }

    public function movement()
    {
        $user = '';
        $bill  = Bill::where([''=>''])->get();
    }

    public function getPrice(Request $request)
    {
        $product = Product::find($request->id);
        if($product){
            $price = $product->sell_price;
            return  response()->json(['message'=>'success','price'=>$price]);
        }else{
            return response()->json(['message'=>'fail','price'=>'']);
        }
    }

    public function getProducts()
    {
        $products = Product::where('amount','>','0')->get()->pluck('name','id');
        return  response()->json(['message'=>'success','products'=>$products]);
    }

    public function move()
    {
       $products =  Product::whereHas('bills')->get();
        return view('Front.products.move',compact('products'));
    }
}
