<?php

namespace App\Http\Controllers\Back;
use App\Http\Controllers\Controller;
use App\Http\Requests\Back\CreateUserRequest;
use App\Http\Requests\Back\EditUserRequest;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('type','user')->get();
        return view('Front.users.index',compact('users'));
    }

    public function create()
    {
        return view('Front.users.create');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('Front.users.edit', compact('user'));
    }

    public function store(CreateUserRequest $request)
    {
         User::create($request->validated()+["type"=>'user']);

         return redirect(route('users.index'))->withTrue('تم الإضافة بنجاح');
    }

    public function update(EditUserRequest $request,User $user)
    {
         $user->update($request->validated());
         return redirect(route('users.index'))->withTrue('تم التعديل بنجاح');

    }

    public function show($id){

    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['value' => 1]);

    }

    public function move()
    {
        $users  = User::get()->pluck('id','name');
        return view('',compact('users'));
    }

    public function pay()
    {
       return dd('1');
    }

}
