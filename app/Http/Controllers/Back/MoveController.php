<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class MoveController extends Controller
{
    public function productMove()
    {
        $products = Product::whereHas('bills')->get();
        return view('Front.products.move',compact('products'));
    }

    public function userMove()
    {
        $users = User::where('type','user')->whereHas('bills')->get();
        return view('Front.users.move',compact('users'));
    }

    public function providerMove()
    {
        $users = User::where('type','provider')->whereHas('bills')->get();
        return view('Front.providers.move',compact('users'));
    }
}
