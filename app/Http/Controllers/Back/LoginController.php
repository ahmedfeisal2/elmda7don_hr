<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
        return view('Front.auth.login');
    }

    public function auth(Request $request)
    {
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password], true)) {
            return redirect()->route('app.home')->withInfo('مرحبا بك مره آخري');
        }else{
            return redirect()->route('login')->withFalse('بيانات غير صحيحه');
        }
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('login')->withInfo('تم تسجيل الخروج');
    }
}
