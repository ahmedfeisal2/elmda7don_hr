<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\Back\PaymentRequest;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function userPay()
    {
        $usersId = User::where('type','user')->get()->pluck('id');
        $payments = Payment::whereIn('user_id',$usersId)->get();
        $type ='user';
        return view('Front.payments.index',compact('payments','type'));
    }

    public function userPayCreate()
    {
        $users = User::where('type','user')->get()->pluck('name','id');
        return view('Front.payments.create',compact('users'));
    }

    public function userPayStore(PaymentRequest $request)
    {
        $inputs = array_except($request->validated(), ['user_id','money_before','money_paid']);
        $user = User::find($request->user_id);
        $inputs['user_id']      = $request->user_id;
        $inputs['money_before'] = $user->total_money;
        $inputs['money_after']  = $user->total_money  - $request->money_paid;
        $inputs['money_paid']   = $request->money_paid;
        Payment::create($inputs);
        $user->total_money     -= $request->money_paid;
        $user->save();

        return redirect()->route('pay.users');
    }

    public function userPayEdit($id)
    {
        $payment =  Payment::find($id);
        $users = User::where('type','user')->get()->pluck('id','name');
        return view('Front.payments.edit',compact('users','payment'));
    }

    public function userPayUpdate(PaymentRequest $request,$id)
    {

        return redirect()->route('pay.users');
    }


    public function userPayDestroy($id)
    {
       $payment = Payment::find($id);
       $payment->delete();
       return response()->json(['value' => 1]);
    }




    /*****************************  Provider *******************************/
    public function providerPay()
    {
        $usersId = User::where('type','provider')->get()->pluck('id');
        $payments = Payment::whereIn('user_id',$usersId)->get();
        $type ='provider';
        return view('Front.provider_payments.index',compact('payments','type'));
    }

    public function providerPayCreate()
    {
        $users = User::where('type','provider')->get()->pluck('name','id');
        return view('Front.provider_payments.create',compact('users'));
    }

    public function providerPayStore(PaymentRequest $request)
    {

        $inputs = array_except($request->validated(), ['user_id','money_before','money_paid']);
        $user = User::find($request->user_id);
        $inputs['user_id']      = $request->user_id;
        $inputs['money_before'] = $user->total_money;
        $inputs['money_after']  = $user->total_money  - $request->money_paid;
        $inputs['money_paid']   = $request->money_paid;
        Payment::create($inputs);
        $user->total_money     -= $request->money_paid;
        $user->save();

        return redirect()->route('pay.providers')->withTrue('تم إضافة المقبوضات بنجاح');
    }

    public function providerPayEdit($id)
    {
        $payment = Payment::find($id);
        $users = User::where('type','provider')->get()->pluck('name','id');
        return view('Front.provider_payments.edit',compact('payment','users'));
    }

    public function providerPayUpdate(PaymentRequest $request, $id)
    {
        return redirect()->route('pay.providers');
    }

    public function providerPayDestroy($id)
    {
       $payment = Payment::find($id);
       $payment->delete();
       return response()->json(['value' => 1]);
    }


    public function total(Request $request)
    {
        $user = User::find($request->id);
        if($user){
            $total = $user->total_money;
            if($total){
                return  response()->json(['message'=>'success','total'=>$total]);
            }else{
                return response()->json(['message'=>'fail','price'=>'']);
            }
        }else{
            return response()->json(['message'=>'fail','price'=>'']);
        }
    }

}
