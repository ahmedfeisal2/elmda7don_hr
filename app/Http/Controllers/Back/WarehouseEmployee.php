<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warehouse;
use App\Http\Requests\Back\WarehouseEmployeeRequest;

class WarehouseEmployee extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->path = 'Front.warehouse_employees.';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Warehouse::all();
        return view($this->path.'index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseEmployeeRequest $request)
    {

        $inputs = $request->validated();
        if($request->image){
            $inputs['image'] = uploaded($request->image,(new Warehouse)->getTable());
        }
        if($request->password){
            $inputs['password']=bcrypt($request->password);
        }
        $employee = Warehouse::create($inputs);

        return redirect(route('warehouse_employees.index'))->withTrue('تم تسجيل الموظف بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Warehouse::findOrFail($id);
        return view($this->path.'edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WarehouseEmployeeRequest $request, $id)
    {
        $employee = Warehouse::findOrFail($id);

      $inputs = $request->validated();
      if($request->image){
          $inputs['image'] = uploaded($request->image,(new Warehouse)->getTable());
      }
      if($request->password){
          $inputs['password']=bcrypt($request->password);
      }else {
        unset($inputs['password']);
      }
      $employee->update($inputs);

      return redirect(route('warehouse_employees.index'))->withTrue('تم تعديل البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = Warehouse::findOrFail($id);
      $user->delete();
      return response()->json(['value' => 1]);
    }
}
