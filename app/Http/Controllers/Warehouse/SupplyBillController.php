<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use Illuminate\Http\Request;

class SupplyBillController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->path = 'warehouse.supply_bills.';
    }
    public function index(){
        $bills = Bill::whereType('supply_bill')->latest()->get();
        return view($this->path.'index',compact('bills'));
    }
}
