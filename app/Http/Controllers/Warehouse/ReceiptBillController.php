<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\Warehouse\ReceiptBill\ReceiptBillRequest;
use App\Models\Bill;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ReceiptBillController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->path = 'warehouse.receipt_bills.';
    }

    public function index()
    {
        $bills = Bill::whereType('receipt_bill')->latest()->get();
        return view($this->path . 'index', compact('bills'));
    }

    public function create()
    {
        $users = User::where('type', 'provider')->get()->pluck('name', 'id');
        $latest_bill = Bill::latest()->first();
        if (!is_null($latest_bill)) {
            $code = $latest_bill->id + 1;
        } else {
            $code = 1;
        }
        return view($this->path . 'create', compact('users', 'code'));
    }

    public function store(ReceiptBillRequest $request)
    {
        \DB::beginTransaction();
        try {
            $inputs = array_except($request->validated(), ['products']);
            $inputs['createor_id'] = auth()->guard('warehouse')->id();
            $inputs['type'] = 'receipt_bill';
            $inputs['status'] = 'done';
            $inputs['date'] = now()->format('Y-m-d');
            $bill = Bill::create($inputs);
            $bill->items()->createMany($request->products);
            $supply_bill = Bill::find($request->bill_id);
            $supply_bill->update(['status' => 'done']);
            foreach ($request->products as $key) {
                $product = Product::find($key['product_id']);
                $product->amount += $key['quantity'];
                $product->update();
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return back()->withFalse('حدث خطأ أثناء التسجيل الرجاء المحاولة مرة أخري');
        }
        return redirect()->route('warehouse.receipt_bills.index')->withTrue('تم إضافة الفاتورة بنجاح');

    }
}
