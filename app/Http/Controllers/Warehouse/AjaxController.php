<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Product;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function get_bills($user_id)
    {
        $bills = Bill::where('user_id', $user_id)->where('date', '<=', now()->format('Y-m-d'))
            ->where('status', 'pending')->pluck('id');
        $view = view('warehouse.receipt_bills.ajax.get_bills', compact('bills'))->render();
        return response()->json(['value' => 1, 'view' => $view]);
    }

    public function get_products($bill_id)
    {
        $bill = Bill::find($bill_id);
        $items = $bill->items;
        $view = view('warehouse.receipt_bills.ajax.get_products', compact('items'))->render();
        return response()->json(['value' => 1, 'view' => $view]);
    }
}
