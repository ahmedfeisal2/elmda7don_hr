@extends('warehouse.layouts.master')

@section('title', 'لوحة تحكم المخازن ')

@section('styles')
<style>
    a.card{
        height: 200px;
    }

    a.card  .card-body{
       padding:  1.75rem;
    }
    a.card .card-body i {
        font-size: 2.7rem;
        margin-bottom: 1rem;
    }



</style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>رئيسية التحكم</h1>
            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                    <li class="breadcrumb-item active" aria-current="page">الرئيسية</li>
                </ol>
            </nav>
            <div class="separator mb-5"></div>
        </div>
        {{--  <div class="col-md-12 col-lg-12 col-xl-12">
           <div class="row">


                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Alarm"></i>
                                <p class="lead card-text ">آقسام المنتجات</p>
                                <p class="lead text-center"> {{ \App\Models\Category::count()}} </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Male mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white">{{ \App\Models\Order::count() }} الطلبات</p>
                                        <p class="text-small text-white"></p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class="progress-bar-circle progress-bar-banner position-relative"
                                         data-color="white" data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="{{ \App\Models\Order::count() }}"
                                         aria-valuemax="1000" data-show-percent="false">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Basket-Coins"></i>
                                <p class="lead"> الآصناف </p>
                                <p class="lead text-center">{{ \App\Models\Product::count()}}</p>
                            </div>
                        </a>
                    </div>


                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Arrow-Refresh"></i>
                                <p class="lead"> العملاء  </p>
                                <p class="lead text-center">{{ \App\Models\User::where('type','user')->count() }}</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Bell mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white">{{ \App\Models\Safe::where('type','incomm')->count() }}  الإيداعات</p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class="progress-bar-circle progress-bar-banner position-relative"
                                         data-color="white" data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="{{ \App\Models\Safe::where('type','incomm')->count() }}"
                                         aria-valuemax="1000" data-show-percent="false">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Mail-Read"></i>
                                <p class="lead ">الموردين </p>
                                <p class="lead text-center">{{ \App\Models\User::where('type','provider')->count() }}</p>
                            </div>
                        </a>
                    </div>
           </div>


                <div class="row">
                    <div class="col-md-12 mb-4 mt-4">
                        <div class="card">
                            <div class="position-absolute card-top-buttons">

                                <button class="btn btn-header-light icon-button" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="simple-icon-refresh"></i>
                                </button>

                                <div class="dropdown-menu dropdown-menu-right mt-3">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Orders</a>
                                    <a class="dropdown-item" href="#">Refunds</a>
                                </div>

                            </div>

                            <div class="card-body">
                                <h5 class="card-title">الطلبات</h5>
                                <div class="dashboard-line-chart">
                                    <canvas id="salesChart1"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>  --}}


    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12  mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">
                        مرحبا بك ف لوحة تحكم المخازن
                        <span>
                            <i class="simple-icon-user"></i>
                            {{ auth()->guard('warehouse')->user()->name }}
                        </span>
                    </h5>
                    <div class="content_home">
                        <ul>
                            <li><a class="shadow p-3 mb-5 bg-white rounded"
                                   href="{{ route('warehouse.supply_bills.index') }}"><i class="simple-icon-list"></i>
                                    فواتير التوريد</a></li>
                            <li><a class="shadow p-3 mb-5 bg-white rounded"
                                   href="{{ route('warehouse.receipt_bills.index') }}"><i class="simple-icon-list"></i>
                                    فواتير الاستلام</a></li>
                            <li><a class="shadow p-3 mb-5 bg-white rounded"
                                   href="{{ route('warehouse.receipt_bills.create') }}"><i class="simple-icon-plus"></i>
                                    اضافة فاتورة استلام جديدة</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('js')
    <script src="{{ asset('js/dore.script.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script>
    </script>
@stop
