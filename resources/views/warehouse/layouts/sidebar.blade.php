<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="active"><a href="{{ route('warehouse.home') }}"><i
                            class="iconsmind-Shop-4"></i><span>الرئسية</span></a></li>
                <li><a href="{{ route('warehouse.supply_bills.index') }}"><i class="simple-icon-docs"></i><span>فواتير التوريد</span></a>
                </li>
                <li><a href="#bills"><i class="iconsmind-Cash-register2"></i> فواتير الاستلام</a></li>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="bills">
                <li><a href="{{ route('warehouse.receipt_bills.index') }}"><i class="simple-icon-basket-loaded"></i> عرض
                        فواتير الاستلام</a></li>
                <li><a href="{{ route('warehouse.receipt_bills.create') }}"><i class="simple-icon-plus"></i> اضافة
                        فاتورة استلام جديدة</a></li>
            </ul>
        </div>
    </div>
</div>
