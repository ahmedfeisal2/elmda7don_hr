<table class="table table-bordered table-hover" id="tab_logic">
    <thead>
    <tr>
        <th class="text-center"> #</th>
        <th class="text-center"> كود المنتج</th>
        <th class="text-center" style="width: 40%;"> اسم المنتج</th>
        <th class="text-center"> الكمية</th>
    </tr>
    </thead>
    <tbody style="text-align: center;">
    @foreach($items as $key)
        <tr id="addr{{$loop->iteration}}" class="delete_row" onclick="delete_row(event)">
            <td>{{ $loop->iteration }}</td>
            <td style="width: 10%;">
                <span class="code form-control" readonly>{{ $key->product->code??'لا يوجد كود لهذا المنتج' }}</span>
            </td>
            <td style="width: 50%;">
                <span class="type form-control" readonly>{{ $key->product->name }}</span>
            </td>
            <input type="hidden" name="products[{{$loop->iteration}}][product_id]" value="{{ $key->product_id }}"
                   id="product_{{$loop->iteration}}">
            <td><input type="number" name='products[{{$loop->iteration}}][quantity]' value="{{ $key->quantity }}"
                       required placeholder='ادخل الكمية' class="form-control qty" onKeyPress="quantity_key(event)"
                       id="qunatity_{{$loop->iteration}}" tabindex="2" step="0" min="0"/></td>
            <input type="hidden" value="{{ $key->price }}" name='products[{{$loop->iteration}}][price]'
                   class="form-control price" readonly/>
            <input type="hidden" name='products[{{$loop->iteration}}][q_price]' value="{{ $key->q_price }}"
                   placeholder='0.00' class="form-control total" readonly/>
        </tr>
        <script>
            qunatity_increase ={{ $loop->iteration+1 }};
        </script>
    @endforeach
    </tbody>
</table>
