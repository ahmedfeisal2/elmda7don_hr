<select class="form-control select2-single subs" name="bill_id" id="bill_id" required>
    @foreach($bills as $id)
        <option value="{{$id}}">{{ $id }}</option>
    @endforeach
</select>

<script>
    $('.subs').select2({
        theme: "bootstrap",
        placeholder: "",
        maximumSelectionSize: 6,
        containerCssClass: ":all:"
    });
</script>
<script>

    $(document).ready(function () {
        var type = $('.subs').children("option:selected").val();
        getAjaxResponse("{{ url('warehouse/ajax/get_products') }}/" + type, 'product_table')
        $('.subs').on('change', function () {
            var type = $(this).children("option:selected").val();
            getAjaxResponse("{{ url('warehouse/ajax/get_products') }}/" + type, 'product_table')
        });
    });

    // ================= Functions ======================
    function getAjaxResponse(route, place) {
        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            success: function (data) {
                if (data['value'] == 1) {
                    $('.' + place).html(data['view']);
                }
            }
        });
    }
</script>
