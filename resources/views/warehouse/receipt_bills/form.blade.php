@section('js')
    <script>
        // ====== Print

        function printDiv(divName) {
            var printContents = document.getElementById('print_bill').innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            // document.body.innerHTML = originalContents;
        }
    </script>
    <script>
        var qunatity_increase = 1;
        var src = "{{ url('ajax/search_products') }}";
        $(document).ready(function () {

            // for first search
            $('.autocomplete').autocomplete({
                serviceUrl: "{{ route('search_products') }}",
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: 'لا توجد منتجات لهذا البحث',
                transformResult: function (response) {

                    return {
                        suggestions: $.map(JSON.parse(response), function (dataItem) {

                            return {code: dataItem.code, value: dataItem.name, data: dataItem.price, id: dataItem.id};
                        })
                    };
                },
                onSelect: function (suggestion) {
                    $('#tab_logic tbody').append(`
                          <tr id="addr${qunatity_increase}" class="delete_row" onclick="delete_row(event)">
                              <td>${qunatity_increase}</td>
                              <td style="width: 10%;">
                                  <span class="code form-control" readonly>كود المنتج</span>
                              </td>
                              <td style="width: 50%;">
                                  <span class="type form-control" readonly>إسم المنتج</span>
                              </td>
                              <input type="hidden" name="products[${qunatity_increase}][product_id]" id="product_${qunatity_increase}">
                              <td><input type="number" name='products[${qunatity_increase}][quantity]' required placeholder='ادخل الكمية' class="form-control qty" onKeyPress="quantity_key(event)" id="qunatity_${qunatity_increase}"  tabindex="2" step="0" min="0"/></td>
                          </tr>
                      `);
                    $("#addr" + (qunatity_increase) + " .code").text("" + suggestion.code);
                    $("#addr" + (qunatity_increase) + " .type").text("" + suggestion.value);

                    $("#product_" + qunatity_increase).val("" + suggestion.id);
                    $('#qunatity_' + qunatity_increase).focus();
                    $(".code_search").val('');
                }
            });

            $('#tab_logic tbody').on('keyup change', function () {
                calc();
            });
            $('#tax').on('keyup change', function () {
                calc_total();
            });


        });

        function addInput(e) {
            $('.autocomplete').val('');
            $('.code').focus();
            qunatity_increase++;

            $('.autocomplete').autocomplete({
                serviceUrl: "{{ route('search_products') }}",
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: 'لا توجد منتجات لهذا البحث',
                transformResult: function (response) {

                    return {
                        suggestions: $.map(JSON.parse(response), function (dataItem) {

                            return {code: dataItem.code, value: dataItem.name, data: dataItem.price, id: dataItem.id};
                        })
                    };
                },
                onSelect: function (suggestion) {
                    $('#tab_logic tbody').append(`
                          <tr id="addr${qunatity_increase}" class="delete_row" onclick="delete_row(event)">
                              <td>${qunatity_increase}</td>
                              <td style="width: 10%;">
                                  <span class="code form-control" readonly>كود المنتج</span>
                              </td>
                              <td style="width: 50%;">
                                  <span class="type form-control" readonly>إسم المنتج</span>
                              </td>
                              <input type="hidden" name="products[${qunatity_increase}][product_id]" id="product_${qunatity_increase}">
                             <td> <input type="number" name='products[${qunatity_increase}][quantity]' required placeholder='ادخل الكمية' class="form-control qty" onKeyPress="quantity_key(event)" id="qunatity_${qunatity_increase}"  tabindex="2" step="0" min="0"/></td>
                          </tr>
                      `);
                    $("#addr" + (qunatity_increase) + " .code").text("" + suggestion.code);
                    $("#addr" + (qunatity_increase) + " .type").text("" + suggestion.value);
                    $("#product_" + qunatity_increase).val("" + suggestion.id);
                    $('#qunatity_' + qunatity_increase).focus();
                    $(".code_search").val('');
                }
            })


        }

        function quantity_key(e) {
            if (e.which == 13) {
                e.preventDefault();
                addInput(e)
            }
        }

        // Delete Row

        function delete_row(e) {
            // document.getElementById("addr0").style.backgroundColor = "#4a4a4a";
            // document.getElementById("addr0").style.color = "#fff";

            $('.delete_row').click(function () {
                $(this).addClass('deleted_row');
                $("#myModal").modal();
            });

        }

        function cancel_deleted_key(e) {


        }

        $(this).css({
            'background-color': '#4a4a4a',
            'color': 'white',
        });
        $(document).ready(function () {
            $(window).keydown(function (event) {

                if (event.keyCode == 96) {
                    // document.getElementById('addr0').remove();
                    var deletemodal = document.querySelectorAll("div[class^='modal-backdrop fade show']");
                    var deletemodals = deletemodal[0];
                    deletemodals.remove();
                    $('.modal.fade.show').removeClass('show');
                }
                if (event.keyCode == 96) {
                    // document.getElementById('addr0').remove();
                    var deleterow = document.querySelectorAll("tr[class^='delete_row deleted_row']");
                    var deleterows = deleterow[0];
                    deleterows.remove();
                    $('.modal.fade.show').removeClass('show');
                }
                // return false;
            });
        });
        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 27) {
                    var liElements = document.querySelectorAll("div[class^='modal-backdrop fade show']");
                    var nonExistentFirstElement = liElements[0];
                    nonExistentFirstElement.remove();
                    $('.modal.fade.show').removeClass('show');
                    $('.delete_row').removeClass('deleted_row');
                }

                // return false;
            });
        });

        // ==================================
        function calc() {
            $('#tab_logic tbody tr').each(function (i, element) {
                var html = $(this).html();
                if (html != '') {
                    var qty = $(this).find('.qty').val();
                    var price = $(this).find('.price').val();
                    $(this).find('.total').val(qty * price);

                    calc_total();
                }
            });
        }

        function calc_total() {
            total = 0;
            $('.total').each(function () {
                total += parseInt($(this).val());
            });
            $('#sub_total').val(total.toFixed(2));
            tax_sum = total / 100 * $('#tax').val();
            // $('#tax_amount').val(tax_sum.toFixed(2));
            $('#total_amount').val((tax_sum - total).toFixed(2));
        }


    </script>
    <script>
        $(document).ready(function () {

            var user_id = $('.main_category').children("option:selected").val();
            getAjaxResponse("{{ url('warehouse/ajax/get_bills') }}/" + user_id, 'bill')
            $('.main_category').on('change', function () {
                var type = $(this).children("option:selected").val();
                getAjaxResponse("{{ url('warehouse/ajax/get_bills') }}/" + type, 'bill')
            });
        });


        // ================= Functions ======================
        function getAjaxResponse(route, place) {
            $.ajax({
                url: route,
                method: "GET",
                dataType: "json",
                success: function (data) {
                    if (data['value'] == 1) {
                        $('.' + place).html(data['view']);
                    }
                }
            });
        }
    </script>
@endsection
<section class="billing_section" id="print_bill">

    <div class="">
        <div class="head-billing">
            <form action="{{ route('warehouse.receipt_bills.store') }}" method="post">
                @csrf
                <div class="row" style="border-bottom: 1px solid #CCCCCC; padding-bottom: 15px; margin-bottom: 15px;">

                    <div class="col-md-4">
                        <div class="form-group col-custom">
                            <label class="">إسم العميل /</label>
                            <select class="form-control select2-single main_category" required name="user_id"
                                    id="category_id">
                                @foreach($users as $id=>$name)
                                    <option value="{{ $id }}"> {{ $name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-custom">
                            <label class="bill_id">رقم فاتورة امر التوريد /</label>
                            <div class="bill">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="logo_biiling">

                            <img src="{{ asset('img/logo/logo.png') }}" alt=""/>
                            <h1>فاتورة استلام</h1>
                            @if(isset($code))
                                <p class="text-center"><span class="code_bi">فاتورة استلام رقم {{ $code }}</span></p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group col-custom">
                            <label class="">تاريح و وقت انشاء الفاتورة /</label>
                            <span class="form-control">
                            {{ now()->format('Y-m-d') }}
                            ( {{ now()->format('h:i a') }} )
                        </span>
                        </div>
                    </div>

                </div>

                <div class="row clearfix">


                    <div class="col-md-8">
                        {{--                        <div class="form-group print-custom">--}}
                        {{--                            <label class="">إضافة منتج إلي الفاتورة</label>--}}
                        {{--                            <input type="text" placeholder='ادخل الكود' tabIndex="1"--}}
                        {{--                                   class="form-control code code_search autocomplete">--}}
                        {{--                        </div>--}}
                        <div class="table-height product_table">
                            <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                <tr>
                                    <th class="text-center"> #</th>
                                    <th class="text-center"> كود المنتج</th>
                                    <th class="text-center" style="width: 40%;"> اسم المنتج</th>
                                    <th class="text-center"> الكمية</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center;">

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <input type="hidden" class="form-control" id="tax" placeholder="0">
                    <input type="hidden" name='total' id="total_amount" placeholder='0.00' class="form-control"/>
                    <div class="col-md-4 col-custom">
                        <label class=""> الملاحظات /</label>
                        <textarea class="form-control" name="notes" cols="3" rows="5"
                                  placeholder="الملاحظات"></textarea>
                    </div>
                </div>
            </form>
            <div class="row clearfix print-custom">
                <div class="col-md-12">
                    <a style="color: #fff;" onclick="printDiv('print_bill')" class="btn btn-primary">
                        <i class="simple-icon-printer"></i>
                        طباعة
                    </a>
                    <button type="submit" class="btn btn-primary" id="form_submit">حفظ</button>

                </div>
            </div>
        </div>
    </div>

    @include('Front.bills.delete_modal')

</section>
