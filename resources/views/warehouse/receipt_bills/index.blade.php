@extends('warehouse.layouts.master')

@section('title', 'فواتير التوريد')
@section('styles')
    <style>
        div.dataTables_filter {
            text-align: left;
            float: left;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>فواتير الاستلام</h1>
                    <div class="float-sm-right text-zero">
                        <a href="{{route('warehouse.receipt_bills.create')}}" class="btn btn-primary btn-lg mr-1">إضافة
                            فاتورة استلام جديدة</a>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">فواتير الاستلام</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">عرض كل الفواتير</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="separator mb-5"></div>
        </div>
    </div>

    <div class="row">

        <div class="col-12 list">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title"> الفواتير</h5>

                    <table id="example" style="width:100%"
                           class="dataTables table table-striped table-bordered cell-border compact stripe">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center"> المورد</th>
                            <th class="text-center">كود الفاتورة</th>
                            <th class="text-center">كود أمر التوريد</th>
                            <th class="text-center"> تاريخ الفاتورة</th>
                            <th class="text-center"> عدد المنتجات</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bills as $bill)
                            <tr class="{{ $bill->id }}">
                                <td class="text-center"> {{ $loop->iteration }} </td>
                                <td>
                                    <p class="text-muted"> {{$bill->user->name}} </p>
                                </td>
                                <td class="text-center">{{ $bill->id }} </td>
                                <td class="text-center">{{ $bill->bill_id }} </td>

                                <td class="text-center">
                                    <span
                                        class="badge badge-pill badge-success">{{ date('Y-m-d', strtotime( $bill->date)) }}</span>
                                </td>
                                <td class="text-center">
                                    <p class="text-muted"> {{$bill->products->count()}} </p>
                                </td>


                            </tr>


                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @include('Front.includes.delete_model')
@stop
@section('js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                keys: true,
                scrollX: true,
                autoheight: true,
            });
        });
    </script>
@endsection
