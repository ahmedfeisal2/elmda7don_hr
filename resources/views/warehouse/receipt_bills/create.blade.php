@extends('warehouse.layouts.master')
@section('title', 'اضافة فاتورة استلام جديدة')
@section('styles')
    <style>
        .product-td {
            width: 400px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>رئيسية التحكم</h1>
            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">الرئيسية</a></li>
                    <li class="breadcrumb-item"><a href="#">الفواتير</a></li>
                    <li class="breadcrumb-item active" aria-current="page">اضافة فاتورة استلام جديدة</li>
                </ol>
            </nav>
            <div class="separator mb-5"></div>
        </div>
        <div class="col-12">


            <div class="card mb-4">
                <div class="card-body">
                    <form action="{{ route('warehouse.receipt_bills.store') }}" method="POST">
                        @csrf
                        @include('warehouse.receipt_bills.form')
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
