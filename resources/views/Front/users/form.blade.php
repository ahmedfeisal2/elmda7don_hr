<div class="form-group">
    <label for="name">الإسم</label>
    <input name="name" type="text" @isset($user) value="{{$user->name}}" @else value="{{ old('name') }}" @endisset class="form-control @error('name') is-invalid @enderror">
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="form-group">
        <label for="address">العنوان </label>
        <input type="text" name="address" id="address"  @isset($user) value="{{$user->address}}" @endisset class="form-control @error('address') is-invalid @enderror">
        @error('address')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
 </div>

<div class="form-group">
        <label for="company_phone">رقم هاتف الشركة</label>
        <input type="number" name="company_phone" id="company_phone"  @isset($user) value="{{$user->company_phone}}" @endisset class="form-control">
        @error('company_phone')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
</div>


<div class="form-group">
    <label for="personal_phone"> رقم هاتف المنزل </label>
    <input type="personal_phone" name="personal_phone" id="personal_phone" @isset($user) value="{{$user->personal_phone}}" @endisset  class="form-control">
    @error('personal_phone')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>


<div class="form-group">
    <label for="phone"> الموبيل</label>
    <input type="phone" name="phone" id="phone"  @isset($user) value="{{$user->phone}}" @endisset class="form-control">
    @error('phone')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>



<div class="form-group">
    <label for="max_available_money"> آقصي ملبغ للسداد </label>
    <input type="number" name="max_available_money" min="0" id="max_available_money" @isset($user) value="{{$user->max_available_money}}" @endisset class="form-control">
    @error('max_available_money')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>



<div class="form-group">
    <label for="max_available_money"> آقصي مدة للسداد </label>
    <input type="number" name="max_available_date" min="0" id="max_available_date" @isset($user) value="{{$user->max_available_date}}" @endisset class="form-control">
    @error('max_available_date')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>



<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" name="submit" class="btn btn-primary">حفظ</button>
    </div>
</div>
