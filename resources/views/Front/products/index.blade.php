@extends('Front.layouts.master')

@section('title', 'Products')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1> الآصناف  </h1>
                    <div class="float-sm-right text-zero">
                        <a href="{{route('products.create')}}"  class="btn btn-primary btn-lg mr-1">إضافة</a>

                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('app.home') }}">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">الآصناف</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">عرض الكل</li>
                        </ol>
                    </nav>
                </div>

                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title"> المنتجات </h5>
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#                </th>
                                        <th class="text-center">الكود            </th>
                                        <th class="text-center">الإسم             </th>
                                        <th class="text-center">القسم </th>
                                        <th class="text-center">  الكمية         </th>
                                        <th class="text-center"> المكسب   %      </th>
                                        <!-- <th class="text-center"> حركة الصنف      </th> -->
                                        <th class="text-center"> خيارات          </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="{{ $product->id }}">
                                            <td class="text-center"> {{ $loop->iteration }} </td>
                                            <td class="text-center">
                                                <span class="@if(is_null($product->code)) badge badge-danger @endif">{{ is_null($product->code)? 'لا يوجد له كود' : $product->code }}</span>
                                            </td>
                                            <td class="text-center">
                                                <p class="list-item-heading"> {{ $product->name  }}</p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted"> {{$product->category->name}} </p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted"> {{$product->amount}} </p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted"> {{$product->profit_percent  }}</p>
                                            </td>
                                            <!-- <td class="text-center">
                                                <a class="btn btn-info" href="#"> عرض حركة الصنف  </a>
                                            </td> -->
                                            <td class="text-center">
                                                <a href="{{ route('products.edit',[$product->id]) }}">
                                                    <span class="badge badge-pill badge-secondary">تعديل</span>
                                                </a>
                                                <a onclick="deleteItem({{ $product->id }} , '{{ route('products.destroy',$product->id) }}')" >
                                                    <span class="badge badge-pill badge-danger">حذف</span>
                                                </a>
                                            </td>


                                        </tr>


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
