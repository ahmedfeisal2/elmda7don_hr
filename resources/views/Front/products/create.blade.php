@extends('Front.layouts.master')
@section('title', 'إضافة منتج جديدة')
@section('content')
<div class="row">
        <div class="col-12">
                <h1>رئيسية التحكم</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">الرئيسية</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">المنتجات</a></li>
                        <li class="breadcrumb-item active" aria-current="page">إضافة منتج جديد</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
        </div>
        <div class="col-12">
                <div class="card mb-4">
                    <div class="card-body">
                    <form action="{{ route('products.store') }}" method="POST">
                        @csrf
                        @include('Front.products.form')
                    </form>
                    </div>
                 </div>
        </div>
</div>

@endsection
@section('js')
<script>

    $(document).ready(function(){
  $('.main_category').on('change',function(){
      var type = $(this).children("option:selected").val();
      getAjaxResponse("{{ url('/ajax/get_subcategory') }}/"+type,'subcategory')
  });
});
// ================= Functions ======================
        function getAjaxResponse(route,place) {
            $.ajax({
                url: route,
                method: "GET",
                dataType:"json",
                success:function(data){
                    if (data['value'] == 1) {
                        $('.'+place).html(data['view']);
                    }
                }
            });
        }
</script>
@endsection
