<select class="form-control select2-single subscategory" name="category_id" id="category_id" required>
    @foreach($categories as $key => $category)
        <option value="{{$key}}" @isset($product)  @if($key == $product->category_id)  selected @endif @endisset>{{ $category }}</option>
    @endforeach
</select>

<script>
    $('.subscategory').select2({
        theme: "bootstrap",
        placeholder: "",
        maximumSelectionSize: 6,
        containerCssClass: ":all:"
    });
</script>
