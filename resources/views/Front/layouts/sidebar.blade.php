<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="active"><a href="{{ url('/') }}"><i class="iconsmind-Shop-4"></i><span>الرئسية</span></a></li>
                @auth
                 @if (auth()->user()->level == '1')
                    <li><a href="#safes"><i class="iconsmind-Cash-register2"></i> الخزنه</a></li>
                    <li><a href="#warehouse_employees"><i class="simple-icon-layers"></i> موظفين المخازن</a></li>
                 @endif
                @endauth
                <li><a href="#users"><i class="iconsmind-Digital-Drawing"></i> العملاء</a></li>
                <li><a href="#providers"><i class="iconsmind-Digital-Drawing"></i> الموردين</a></li>
                <li><a href="#categories"><i class="iconsmind-Three-ArrowFork"></i> الآقسام الرئيسية</a></li>
                <li><a href="#subcategories"><i class="iconsmind-Pantone"></i> الآقسام الفرعيه</a></li>
                <li><a href="#products"><i class="iconsmind-Space-Needle"></i> الآصناف</a></li>
                <li><a href="#bills"><i class="iconsmind-Air-Balloon"></i> الفواتير</a></li>
                {{--  <li><a href="#reports"><i class="iconsmind-Pantone"></i> التقارير</a></li>  --}}
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            @auth
            @if (auth()->user()->level == '1')
                <ul class="list-unstyled" data-link="safes">
                    <li><a href="{{ route('save.index') }}"><i class="simple-icon-credit-card"></i> عرض الخزنه</a></li>
                    <li><a href="{{ route('save.create') }}"><i class="simple-icon-list"></i> إضافة للخزنه</a></li>
                </ul>
                <ul class="list-unstyled" data-link="warehouse_employees">
                    <li><a href="{{ route('warehouse_employees.index') }}"><i class="simple-icon-list"></i> عرض الموظفين</a></li>
                   <li><a href="{{ route('warehouse_employees.create') }}"><i class="simple-icon-user-follow"></i> إضافة موظف جديد</a></li>
                </ul>
            @endif
            @endauth
            <ul class="list-unstyled" data-link="users">
                <li><a href="{{ route('users.index') }}"><i class="simple-icon-credit-card"></i> عرض العملاء</a></li>
                <li><a href="{{ route('move.user')  }}"><i class="simple-icon-list"></i> حركة العملاء</a></li>
                <li><a href="{{ route('pay.users') }}"><i class="simple-icon-list"></i> مقوبضات </a></li>
                {{--  <li><a href="#"><i class="simple-icon-list"></i> بحث متقدم </a></li>  --}}
                <li><a href="{{ route('users.create') }}"><i class="simple-icon-list"></i> إضافة عميل</a></li>

            </ul>
            <ul class="list-unstyled" data-link="providers">
                <li><a href="{{ route('providers.index') }}"><i class="simple-icon-credit-card"></i> عرض الموردين</a></li>
                <li><a href="{{ route('move.provider') }}"><i class="simple-icon-list"></i> حركة الموردين</a></li>
                <li><a href="{{ route('pay.providers') }}"><i class="simple-icon-list"></i> مقوبضات </a></li>
                <li><a href="{{ route('provider.short') }}"><i class="simple-icon-list"></i> نواقص مورد</a></li>
                <li><a href="{{ route('providers.create') }}"><i class="simple-icon-list"></i> إضافة مورد</a></li>
            </ul>
            <ul class="list-unstyled" data-link="categories">
                <li><a href="{{ route('categories.index') }}"><i class="simple-icon-docs"></i> عرض الآقسام الرئيسية</a></li>
                <li><a href="{{ route('categories.create') }}"><i class="simple-icon-doc"></i> إضافة قسم رئيسي</a></li>
            </ul>
            <ul class="list-unstyled" data-link="subcategories">
                <li><a href="{{ route('subcategories.index') }}"><i class="simple-icon-docs"></i> عرض الآقسام الفرعية</a></li>
                <li><a href="{{ route('subcategories.create') }}"><i class="simple-icon-doc"></i> إضافة قسم  فرعي</a></li>
            </ul>
                <ul class="list-unstyled" data-link="products">
                    <li><a href="{{ route('products.index') }}"><i class="simple-icon-bell"></i> عرض الآصناف</a></li>
                    <li><a href="{{ route('move.product') }}"><i class="simple-icon-list"></i> حركة صنف</a></li>
                    <li><a href="{{ route('products.create') }}"><i class="simple-icon-badge"></i> إضافة صنف</a></li>
                </ul>
                <ul class="list-unstyled" data-link="bills">
                    <li><a href="{{ route('supply_bills.index') }}"><i class="simple-icon-basket-loaded"></i> عرض فواتير
                            التوريد</a></li>
                    <li><a href="{{ route('supply_bills.create') }}"><i class="simple-icon-plus"></i> اضافة امر توريد
                            جديد</a></li>
                </ul>
                {{--                <ul class="list-unstyled" data-link="bills">--}}
                {{--                    <li><a href="#"><i class="simple-icon-basket-loaded"></i> عرض فواتير العملاء</a></li>--}}
                {{--                    <li><a href="{{ route('viewUsersBills') }}"><i class="simple-icon-pie-chart"></i> إضافة فاتورة عميل</a></li>--}}
                {{--                </ul>--}}
                {{--            <ul class="list-unstyled" data-link="bills">--}}
                {{--                <li><a href="#"><i class="simple-icon-basket-loaded"></i> عرض فواتير صيانة</a></li>--}}
                {{--                <li><a href="#"><i class="simple-icon-pie-chart"></i> إضافة فاتورة صيانة</a></li>--}}
                {{--            </ul>--}}

        </div>
    </div>
</div>
