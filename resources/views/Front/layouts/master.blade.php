<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title> مؤسسة المتحدون الحديثة || @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="http-root" content="{{ url(Request::root()) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.laravel ={!! json_encode(['csrfToken' => csrf_token(),]) !!} </script>

    <link rel="stylesheet" href="{{ asset('font/iconsmind/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('font/simple-line-icons/css/simple-line-icons.css') }}" />

    <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/fullcalendar.min.css') }}" />
    {{--  <link rel="stylesheet" href="{{ asset('css/vendor/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/datatables.responsive.bootstrap4.min.css') }}" />  --}}
    {{--  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">  --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/vendor/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/owl.carousel.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap-stars.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/nouislider.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/dore.light.blue.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap-datepicker3.min.css') }}" />
    <!-- font AwaDy -->
    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap&subset=arabic" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" media="print" href=" {{ asset('css/print.css')  }} ">
    @yield('styles')
    @yield('prejs')
    <script src="{{ asset('js/vendor/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/vendor/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('js/vendor/chartjs-plugin-datalabels.js') }}"></script>
    <script src="{{ asset('js/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('js/vendor/fullcalendar.min.js') }}"></script>
    {{--  <script src="{{ asset('js/vendor/datatables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'"></script>  --}}
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
    <script src="{{ asset('js/vendor/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/vendor/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/vendor/progressbar.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.barrating.min.js') }}"></script>
    <script src="{{ asset('js/vendor/select2.full.js') }}"></script>
    <script src="{{ asset('js/vendor/nouislider.min.js') }}"></script>
    <script src="{{ asset('js/vendor/bootstrap-datepicker.js') }}"></script>
    <script src="{{asset('js/vendor/bootstrap-notify.min.js')}}"></script>
    <script src="{{ asset('js/vendor/Sortable.js') }}"></script>
    <script src="{{ asset('js/vendor/mousetrap.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('doreadmin-110')}}/jquery.autocomplete.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/9729/zipcodesDK.js"></script>
    <script src="{{asset('js')}}/dore.script.js"></script>
    @yield('js')
</head>

<body id="app-container" class="menu-default show-spinner">

@include('Front.layouts.navbar')
@include('Front.layouts.sidebar')

<main>
    <div class="container-fluid">
        @yield('content')
    </div>
</main>




@include('Front.includes.load')
@include('Front.includes.alert')

<script>

    var token = document.head.querySelector('meta[name="csrf-token"]').content;
    function deleteItem(itemId,route) {
        $("#modal_default").modal('show');
        $('#item').attr('item-id',itemId);
        $('#item').attr('route',route);
    }

    function deleteModal() {
        var newId=$('#item').attr('item-id');
        var route=$('#item').attr('route');
        $("#modal_default").modal('hide');
        $.ajax({
            url: route,
            method: "DELETE",
            dataType:"json",
            data:{_token:token},
            success:function(data){
                if (data['value'] == 1) {
                    $("."+newId).fadeOut('slow',function(){
                        $(this).remove();
                    });
                }
            }
        });
    }
</script>
</body>

</html>
