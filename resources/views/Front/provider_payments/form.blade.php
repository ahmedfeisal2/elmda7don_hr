<div class="form-group">
    <label for="user_id"> إسم العميل</label>

    <select class="form-control select2-single" name="user_id" id="user_id" >
        <option label="&nbsp;">&nbsp;</option>
        @foreach($users as $key => $user)
            <option value="{{$key}}" @isset($payment)  @if($key == $payment->user_id)  selected @endif @endisset>{{ $user }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="money_before">المديونية </label>
    <input type="number" name="money_before" id="money_before"  @isset($payment) value="{{$payment->money_before}}" @else value="{{ old('money_before') }}" @endisset class="form-control @error('money_before') is-invalid @enderror" readonly>
    @error('money_before')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>


<div class="form-group">
    <label for="money_paid">المدفوع</label>
    <input name="money_paid" type="number" @isset($payment) value="{{$payment->money_paid}}" @else value="{{ old('money_paid') }}" @endisset class="form-control @error('money_paid') is-invalid @enderror">
    @error('money_paid')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>






<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" name="submit" class="btn btn-primary">حفظ</button>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#user_id').on('change',function(e){
        e.preventDefault();
        $.ajax({
            type:'GET',
            url:"{{ url('user/get/total') }}",
            data:{id:$(this).val()},
            success:function(data){
                if(data.message = 'success'){
                    $('#money_before').val(data.total);
                }else{
                    alert('من فضلك إختر آحد المستخدمين');
                }

            }
        });
    });
});

</script>
