@extends('Front.layouts.master')

@section('title', 'Bill')
@section('styles')
<style>
    *, *::before, *::after {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
    @media print {
        .card .card-body {
            padding: 1.75rem;
        }
        .card-body {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }

        .card .card-title {
            margin-bottom: 2rem;
        }

        h4 {
            font-size: 1.15rem;
            ont-family: inherit;
            font-weight: 500;
            line-height: 1.2;
            color: inherit;
            margin-top: 0;
        }
        table {
            border-collapse: collapse;
        }
        .table-bordered {
            width: 100%;
            border: 1px solid #dee2e6;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
            border-collapse: collapse;
        }

        th {
            text-align: inherit;
        }

        .table th, .table td {
            padding: .75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table-bordered thead th, .table-bordered thead td {
            border-bottom-width: 2px;
        }


        .table th, .table td {
            padding: .75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0,0,0,0.05);
        }

        p {
            margin-top: 0;
        }

        p {
            line-height: 1.3rem;
            font-family: "Nunito", sans-serif;
        }

        table p, table h6 {
            margin-bottom: initial;
        }

        .list-item-heading {
            font-size: 1rem;
        }

        .text-muted {
            color: #909090 !important;
        }

        table p, table h6 {
            margin-bottom: initial;
        }

        p {
            font-size: 0.85rem;
            line-height: 1.3rem;
            font-family: "Nunito", sans-serif;
        }

        p {
            margin-top: 0;
        }

        .sidebar{
            display: none;
        }
        .theme-button{
            display: none;
        }
        .breadcrumb-section{
            display: none;
        }

    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row  breadcrumb-section">
            <div class="col-12">
                <div class="mb-2">
                    <h1>الفواتير</h1>
                    <div class="float-sm-right text-zero">
                        {{--  <button class="btn btn-primary btn-lg mr-1" id="print" onclick="myFunction()">طباعة</button>  --}}
                        <button class="btn btn-primary btn-lg mr-1"  onclick="javascript:printPage();">طباعة</button>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">الفواتير</a>
                            </li>
                            {{--  <li class="breadcrumb-item active" aria-current="page" onclick="myFunction()">طباعة</li>  --}}
                            <li class="breadcrumb-item active" aria-current="page" onclick="javascript:printPage();">طباعة</li>

                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>

                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>




        <div class="row print-div">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body" id="printTable">
                                <h4 class="card-title" >
                                    @if($bill->status =='offer')
                                            عرض سعر
                                    @elseif($bill->status =='bill')
                                            فاتورة بيع
                                    @elseif($bill->status =='order')
                                            إذن صرف
                                    @endif
                                </h4>

                                <h4 class="card-title text-center">
                                    Halawa+
                                </h4>
                                <table  style="width:100%" class=" table table-striped table-bordered cell-border compact stripe" >
                                    <thead>
                                    <tr>
                                        <th>كود الفاتورة</th>
                                        <th>حالة الفاتورة</th>
                                        <th> تاريخ الفاتورة </th>
                                        <th> عدد المنتجات </th>
                                        <th> صاحب الفاتورة </th>
                                        <th> الإجمالي </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <tr class="{{ $bill->id }}">

                                            <td>{{ $bill->id }} </td>
                                            <td>
                                                <p class="list-item-heading">
                                                   @if($bill->status =='offer')
                                                        عرض سعر
                                                   @elseif($bill->status =='bill')
                                                        فاتورة بيع
                                                    @elseif($bill->status =='order')
                                                        إذن صرف
                                                   @endif
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ date('Y-m-d', strtotime( $bill->date)) }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$bill->products->count()}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$bill->user->name}} </p>
                                            </td>

                                            <td>
                                                <p class="text-muted"> {{$bill->total}} </p>
                                            </td>

                                        </tr>

                                    </tbody>
                                </table>
                                <table  style="width:100%" class=" table table-striped table-bordered cell-border compact stripe" >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> كود المنتج</th>
                                        <th> إسم المنتج</th>
                                        <th> سعر المنتج </th>
                                        <th> الكمية </th>
                                        <th> سعر الكمية </th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse($bill->products as $product)
                                        <tr class="{{ $product->id }}">
                                            <td> {{ $loop->iteration }} </td>
                                            <td>{{ $product->id }} </td>
                                            <td> {{$product->name }}  </td>
                                            <td>
                                                <p class="text-muted">{{ $product->sell_price }}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted">{{ $product->pivot->quantity }}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted"> {{$product->pivot->q_price}} </p>
                                            </td>



                                        </tr>

                                    @empty
                                        <tr>
                                            <td colspan="6"> لايوجد فواتير  حتي الآن </td>
                                        </tr>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop
@section('prejs')
<script>



         function myFunction() {
            var divContents = document.getElementById("printTable").innerHTML;
            var a = window.open('', '', 'height=800, width=900');
            a.document.write('<html>');
            a.document.write('<body > <h1>Halwa Plus <br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();

            }

        function printPage() { window.print(); }


</script>
@endsection
