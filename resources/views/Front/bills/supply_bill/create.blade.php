@extends('Front.layouts.master')
@section('title', 'اضافة أمر توريد جديد')
@section('styles')
<style>
    .product-td{
        width: 400px;
    }
</style>
@endsection
@section('content')
<div class="row">
        <div class="col-12">
                <h1>رئيسية التحكم</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">الرئيسية</a></li>
                        <li class="breadcrumb-item"><a href="#">الفواتير</a></li>
                        <li class="breadcrumb-item active" aria-current="page">اضافة أمر توريد جديد</li>
                    </ol>
                </nav>
            <div class="separator mb-5"></div>
        </div>
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{ route('supply_bills.store') }}" method="POST">
                    @csrf
                    @include('Front.bills.form')
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning text-white">
                <h5 class="modal-title" id="exampleModalLabel">اضافة منتج جديد</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="add_product" method="POST">
                    @csrf
                    {{--                    <input type="text" name="name" class="form-control input_name" placeholder="اسم المنتج">--}}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="name">الإسم</label>
                            <input name="name" id="name" type="text" @isset($product) value="{{$product->name}}"
                                   @else value="{{ old('name') }}"
                                   @endisset class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="code">الكود</label>
                            <input name="code" id="code" type="text" @isset($product) value="{{$product->code}}"
                                   @else value="{{ old('code') }}"
                                   @endisset class="form-control @error('code') is-invalid @enderror">
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="amount">الكمية</label>
                            <input type="number" name="amount" min="0" id="amount"
                                   @isset($product) value="{{$product->amount}}" @endisset class="form-control">
                            @error('amount')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>


                        <div class="form-group col-md-12">
                            <label for="provider_id"> موردين الصنف </label>

                            <select class="form-control select2-multiple" name="provider_id[]" id="provider_id"
                                    multiple="multiple">
                                @foreach($providers as $key => $provider)
                                    <option value="{{$key}}"
                                            @isset($product)
                                            @if ($product->providers->contains($key))
                                            selected
                                        @endif
                                        @endisset
                                    >
                                        {{ $provider }}
                                    </option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="main_category_id">القسم الرئيسي</label>

                            <select class="form-control select2-single main_category" name="main_category_id"
                                    id="main_category_id">
                                <option label="&nbsp;">يمكنك البحث هنا بإسم القسم</option>
                                @foreach($categories as $key => $category)
                                    <option value="{{$key}}"
                                            @isset($product)  @if($key == $product->category->mainCategory->id)  selected @endif @endisset>{{ $category }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="category_id"> القسم الفرعي</label>
                            <div class="subcategory">
                                <select class="form-control select2-single " name="category_id" id="category_id">
                                    @isset($product)
                                        @foreach($subcategories as $key=>$category)
                                            <option value="{{ $key }}"
                                                    @isset($product)  @if($key == $product->category_id)  selected @endif @endisset> {{ $category}} </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="max_amount"> آقصي حد للبيع </label>
                            <input type="number" name="max_amount" min="0" id="max_amount"
                                   @isset($product) value="{{$product->max_amount}}" @endisset class="form-control">
                            @error('max_amount')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="min_amount"> آقل حدد للبيع </label>
                            <input type="number" name="min_amount" min="0" id="min_amount"
                                   @isset($product) value="{{$product->min_amount}}" @endisset class="form-control">
                            @error('min_amount')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="price"> سعر الشراء </label>
                            <input type="number" name="price" min="0" id="price"
                                   @isset($product) value="{{$product->price}}" @endisset class="form-control">
                            @error('price')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="sell_price"> سعر البيع </label>
                            <input type="number" name="sell_price" min="0" id="sell_price"
                                   @isset($product) value="{{$product->sell_price}}" @endisset class="form-control">
                            @error('sell_price')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                    <!-- <div class="form-group">
    <label for="box_pices_amount">   عبوة الكرتونه </label>
    <input type="number" name="box_pices_amount" min="0" id="box_pices_amount" @isset($product) value="{{$product->box_pices_amount}}" @endisset class="form-control">
    @error('box_pices_amount')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
                        </div>

                        <div class="form-group">
                            <label for="pice_amount">   سعر العبوة </label>
                            <input type="number" name="pice_amount" min="0" id="pice_amount" @isset($product) value="{{$product->pice_amount}}" @endisset class="form-control">
    @error('pice_amount')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
                        </div> -->


                        <div class="form-group col-md-6">
                            <label for="name">بلد المنشآ</label>
                            <input name="country" type="text" @isset($product) value="{{$product->country}}"
                                   @else value="{{ old('country') }}"
                                   @endisset class="form-control @error('country') is-invalid @enderror">
                            @error('country')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name">الموديل</label>
                            <input name="model" type="text" @isset($product) value="{{$product->model}}"
                                   @else value="{{ old('model') }}"
                                   @endisset class="form-control @error('model') is-invalid @enderror">
                            @error('model')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                    <!-- <div class="form-group">
    <label for="name">الوحدة</label>
    <input name="unit" type="text" @isset($product) value="{{$product->unit}}" @else value="{{ old('unit') }}" @endisset class="form-control @error('unit') is-invalid @enderror">
    @error('unit')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
                        </div> -->

                        <div class="form-group col-md-12">
                            <label for="desc">ملاحظات </label>
                            <textarea name="desc" id="desc" class="form-control @error('desc') is-invalid @enderror">
          @isset($product) {{$product->desc}} @else {{ old('desc') }} @endisset
        </textarea>
                            @error('desc')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">الغاء</button>
                <button type="button" class="btn btn-success save_product" data-dismiss="modal">حفظ</button>
            </div>
        </div>
    </div>
</div>
@endsection
