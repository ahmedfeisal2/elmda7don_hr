@extends('Front.layouts.master')

@section('title', 'Bill')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>الفواتير</h1>
                    <div class="float-sm-right text-zero">
                        <a href="{{route('bills.create')}}"  class="btn btn-primary btn-lg mr-1">إضافة</a>

                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">الفواتير</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">البيانات</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>

                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title"> الفواتير</h5>

                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>كود الفاتورة</th>
                                        <th>حالة الفاتورة</th>
                                        <th> تاريخ الفاتورة </th>
                                        <th> عدد المنتجات </th>
                                        <th> صاحب الفاتورة </th>
                                        <th> خيارات </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($bills as $bill)
                                        <tr class="{{ $bill->id }}">
                                            <td> {{ $loop->iteration }} </td>
                                            <td>{{ $bill->id }} </td>
                                            <td>
                                                <p class="list-item-heading">
                                                   @if($bill->status =='offer')
                                                        عرض سعر
                                                   @elseif($bill->status =='bill')
                                                        فاتورة بيع
                                                    @elseif($bill->status =='order')
                                                        إذن صرف
                                                   @endif
                                                </p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ date('Y-m-d', strtotime( $bill->date)) }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$bill->products->count()}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$bill->user->name}} </p>
                                            </td>

                                            <td>
                                                @if($bill->status =='offer')
                                                    <a href="{{ route('bills.change',[$bill->id,'bill']) }}">
                                                    <span class="badge badge-pill badge-success">تحويل لفاتورة بيع</span>
                                                    </a>
                                                @elseif($bill->status =='bill')
                                                    <a href="{{ route('bills.change',[$bill->id,'order']) }}">
                                                      <span class="badge badge-pill badge-success">تحويل  لإذن صرف </span>
                                                    </a>
                                                </a>
                                                @elseif($bill->status =='order')
                                                     <span class="badge badge-pill badge-info"> إذن صرف </span>
                                                @endif
                                                <a href="{{ route('bills.print',[$bill->id]) }}">
                                                    <span class="badge badge-pill badge-warning">طباعة</span>
                                                </a>
                                                <a href="{{ route('bills.edit',[$bill->id]) }}">
                                                    <span class="badge badge-pill badge-secondary">تعديل</span>
                                                </a>

                                                <a onclick="deleteItem({{ $bill->id }} , '{{ route('bills.destroy',$bill->id) }}')" >
                                                    <span class="badge badge-pill badge-danger">حذف</span>
                                                </a>
                                            </td>


                                        </tr>

                                  
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop
@section('js')
<script>
    $(document).ready(function(){
        $('#example').DataTable({
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,
        });
    });
</script>
@endsection
