<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding:40px; text-align: center;">
                <h4>هل تريد تأكيد حذف هذا المنتج من الفاتورة</h4>
                <i class="simple-icon-trash"></i>
                <button onKeyPress="cancel_deleted_key(event)" type="submit" style="margin-top: 25px" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> إلغاء الحذف </button>
            </div>

        </div>

    </div>
</div>
