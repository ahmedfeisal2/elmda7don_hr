<div class="form-group">
    <label for="name">الإسم</label>
    <input name="name" type="text" @isset($subcategory) value="{{$subcategory->name}}" @else value="{{ old('name') }}" @endisset class="form-control @error('name') is-invalid @enderror">
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="form-group">
        <label for="desc">الوصف </label>
        <input type="text" name="desc" id="desc"  @isset($subcategory) value="{{$subcategory->desc}}" @else value="{{ old('desc') }}" @endisset class="form-control @error('desc') is-invalid @enderror">
        @error('desc')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
</div>

<div class="form-group">
    <label for="parent_id">القسم الرئيسي</label>

    <select class="form-control select2-single" name="parent_id" id="parent_id" >
        <option label="&nbsp;">&nbsp;</option>
        @foreach($categories as $key => $category)
            <option value="{{$key}}" @isset($subcategory)  @if($key == $subcategory->parent_id)  selected @endif @endisset>{{ $category }}</option>
        @endforeach
    </select>
</div>






<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" name="submit" class="btn btn-primary">حفظ</button>
    </div>
</div>
