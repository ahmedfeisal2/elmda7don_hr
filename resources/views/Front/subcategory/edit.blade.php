@extends('Front.layouts.master')
@section('title', 'تعديل القسم')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>رئيسية التحكم</h1>
            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">الرئيسية</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('subcategories.index') }}">الآقسام</a></li>
                    <li class="breadcrumb-item active" aria-current="page">تعديل قسم فرعي </li>
                </ol>
            </nav>
            <div class="separator mb-5"></div>
        </div>
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-body">
            <form action="{{ route('subcategories.update', ['subcategory' => $subcategory]) }}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                @include('Front.subcategory.form')
            </form>

            </div>
        </div>
        </div>
        </div>
@endsection
