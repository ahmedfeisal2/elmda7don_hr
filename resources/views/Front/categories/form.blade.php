<div class="form-group">
    <label for="name">الإسم</label>
    <input name="name" type="text" @isset($category) value="{{$category->name}}" @else value="{{ old('name') }}" @endisset class="form-control @error('name') is-invalid @enderror">
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="form-group">
        <label for="desc">الوصف </label>
        <input type="text" name="desc" id="desc"  @isset($category) value="{{$category->desc}}" @endisset class="form-control @error('desc') is-invalid @enderror">
        @error('desc')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
</div>






<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" name="submit" class="btn btn-primary">حفظ</button>
    </div>
</div>
