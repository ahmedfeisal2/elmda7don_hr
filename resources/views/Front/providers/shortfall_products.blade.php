@extends('Front.layouts.master')

@section('title', 'Products')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1> نواقص الموردين  </h1>
                    <div class="float-sm-right text-zero">

                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">الآصناف</a>
                            </li>
                            <li class="breadcrumb-item " aria-current="page">البيانات</li>
                            @isset($user)
                                <li class="breadcrumb-item active" aria-current="page">نواقص المورد {{ $user->name }}</li>
                            @endisset
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>

                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                            @isset($user)
                            <h5 class="card-title"> نواقص  الآصناف للمورد  {{ $user->name }} </h5>
                             @else
                                <h5 class="card-title"> نواقص  الآصناف  </h5>
                             @endisset
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th>رقم الصنف      </th>
                                        <th>الإسم           </th>
                                        <th>الوصف          </th>
                                        <th> القسم الرئيسي </th>
                                        <th>  الكمية       </th>
                                        <th> المكسب   %     </th>
                                        <th> الموردين       </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="{{ $product->id }}">
                                            <td> {{ $loop->iteration }} </td>

                                            <td>
                                                <p class="list-item-heading"> {{ $product->name  }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ $product->desc }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$product->category->name}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$product->amount}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$product->profit_percent  }}</p>
                                            </td>

                                            <td>
                                                @foreach ($product->providers as $item)
                                                   <p class="text-muted"> {{$item->name  }}</p> ||

                                                  
                                                @endforeach
                                            </td>

                                        </tr>


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
