@extends('Front.layouts.master')

@section('title', 'Users')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>قائمة حركات الموردين</h1>
                    <div class="float-sm-right text-zero">
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#"> حركات الموردين </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">البيانات</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>

                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title"> حركات الموردين</h5>
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th>رقم العميل</th>
                                        <th>الإسم</th>
                                        <th>الموبيل</th>
                                        <th>العنوان</th>
                                        <th> عدد الفواتير </th>
                                        <th> قيمة المديونية </th>
                                        <th> تاريخ آخر تحديث للعميل </th>
                                        <th>  آخر تاريخ فاتورة العميل    </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr class="{{ $user->id }}">
                                            <td> {{ $user->id }} </td>
                                            <td>
                                                <p class="list-item-heading"> {{ $user->name  }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ $user->phone }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$user->address}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{$user->bills->count()}}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{$user->total_money}}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted">{{$user->last_date}}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted"> {{ date('Y-m-d', strtotime(  $user->bills->last()->date)) }} </p>
                                            </td>
                                        </tr>


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop
@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
