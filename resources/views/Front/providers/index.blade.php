@extends('Front.layouts.master')

@section('title', 'Providers')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>قائمة الموردين</h1>
                    <div class="float-sm-right text-zero">
                        <a href="{{route('providers.create')}}"  class="btn btn-primary btn-lg mr-1">إضافة</a>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">الموردين</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">البيانات</li>
                        </ol>
                    </nav>
                </div>

                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="list">
                        <div class="card ">
                            <div class="card-body">
                                <h5 class="card-title"> الموردين</h5>
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th>رقم المورد</th>
                                        <th>الإسم</th>
                                        <th>الموبيل</th>

                                        <th> قيمة المديونية </th>
                                        <th> تاريخ آخر تعامل </th>
                                        <th> حركة  المورد     </th>
                                        <th> المقبوضات         </th>
                                        <th>  فاتورة المورد   </th>
                                        <th>  نواقص المورد   </th>
                                        <th> خيارات </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($providers as $provider)
                                        <tr class="{{ $provider->id }}">
                                            <td> {{ $provider->id }} </td>
                                            <td>
                                                <p class="list-item-heading"> {{ $provider->name  }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ $provider->phone }}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted">{{$provider->total_money}}</p>
                                            </td>

                                            <td>
                                                <p class="text-muted">{{$provider->last_date}}</p>
                                            </td>

                                            <td>
                                                <a class="btn btn-primary" href="#">  عرض حركة المورد </a>
                                            </td>


                                            <td>
                                                <a class="btn btn-info" href="#">  مقبوضات  المورد </a>
                                            </td>

                                            <td>
                                                <a class="btn btn-success" href="#">  إضافة فاتورة للمورد </a>
                                            </td>

                                            <td>
                                                <a class="btn btn-warning" href="{{ route('provider.short.one',[$provider->id]) }}">  عرض  نواقص المورد </a>
                                            </td>

                                            <td>
                                                <a href="{{ route('providers.edit',[$provider->id]) }}">
                                                    <span class="badge badge-pill badge-secondary btn btn-lg">تعديل</span>
                                                </a>
                                                <a onclick="deleteItem({{ $provider->id }} , '{{ route('providers.destroy',$provider->id) }}')" >
                                                    <span class="badge badge-pill badge-danger btn btn-lg">حذف</span>
                                                </a>
                                            </td>


                                        </tr>

                              
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop


@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
