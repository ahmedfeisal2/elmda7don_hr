<div class="row">
  <div class="from-group col-md-12">
    <label for="name">نوع الموظف</label>
    <select class="form-control select2-single " name="type" id="type" >
        <option value="manger" @isset($employee)  @if($employee->type == 'manger')  selected @endif @endisset >لديه صلاحية الدخول علي النظام</option>
        <option value="employee" @isset($employee)  @if($employee->type == 'employee')  selected @endif @endisset >موظف مراجعة و ليس له دخول علي النظام</option>
    </select>
  </div>
<div class="form-group col-md-6">
    <label for="name">الإسم</label>
    <input name="name" type="text" @isset($employee) value="{{$employee->name}}" @else value="{{ old('name') }}" @endisset class="form-control @error('name') is-invalid @enderror">
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>
<div class="form-group col-md-6">
    <label for="email"> البريد الالكتروني</label>
    <input type="email" name="email" id="email"  @isset($employee) value="{{$employee->email}}" @else value="{{ old('email') }}" @endisset class="form-control">
    @error('email')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="form-group col-md-6">
    <label for="password"> كلمة المرور</label>
    <input type="password" name="password" id="password"  class="form-control">
    @error('password')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>


<div class="form-group col-md-6">
    <label for="password_confirmation">تأكيد كلمة المرور</label>
    <input type="password" name="password_confirmation" id="password_confirmation"  class="form-control">
    @error('password_confirmation')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>
<div class="form-group col-md-6">
    <label for="phone"> الموبيل</label>
    <input type="phone" name="phone" id="phone"  @isset($employee) value="{{$employee->phone}}" @else value="{{ old('phone') }}" @endisset class="form-control">
    @error('phone')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>


<div class="form-group col-md-6">
        <label for="address">العنوان </label>
        <input type="text" name="address" id="address"  @isset($employee) value="{{$employee->address}}" @else value="{{ old('address') }}" @endisset class="form-control @error('address') is-invalid @enderror">
        @error('address')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
 </div>



<div class="form-group col-md-12">
  @isset($employee)

  <img src="{{ $employee->ImageUrl }}" width="80" height="80" style="margin: 7px;">

  @endisset
<div class="input-group   mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">
            الصورة
        </span>
    </div>
    <div class="custom-file">
        <input type="file" name="image" class="custom-file-input" id="inputGroupFile01">
         <label class="custom-file-label" for="inputGroupFile01">
            اختر الصورة
            </label>
        </div>
    </div>
</div>
</div>
<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" name="submit" class="btn btn-primary">حفظ</button>
    </div>
</div>
