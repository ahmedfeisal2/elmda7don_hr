@extends('Front.layouts.master')

@section('title', 'موظفين المخازن')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>قائمة موظفين المخازن</h1>
                    <div class="float-sm-right text-zero">
                        <a href="{{route('warehouse_employees.create')}}"  class="btn btn-primary btn-lg mr-1">إضافة</a>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">موظفين المخازن</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">عرض الكل</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>
{{--                    <div class="collapse d-md-block" id="displayOptions">--}}
{{--                        <div class="d-block d-md-inline-block">--}}
{{--                            <div class="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">--}}
{{--                                <input placeholder="Search...">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title"> موظفين المخازن</h5>
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">الصورة</th>
                                        <th class="text-center">الإسم</th>
                                        <th class="text-center">البريد الالكتروني</th>
                                        <th class="text-center">الموبيل</th>
                                        <th class="text-center">العنوان</th>
                                        <th class="text-center">صلاحية الدخول للنظام</th>

                                        <th class="text-center"> خيارات </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees as $user)
                                        <tr class="{{ $user->id }}">
                                            <td class="text-center"> {{ $user->id }} </td>
                                            <td class="text-center"> <img src="{{ $user->ImageUrl }}" width="60" height="60"> </td>
                                            <td class="text-center">
                                                <p class="list-item-heading"> {{ $user->name  }}</p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted">{{ $user->email }}</p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted">{{ $user->phone }}</p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted"> {{$user->address}} </p>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-muted"> {{ $user->type == 'manger'?'نعم':'لا - موظف مراجعة'}} </p>
                                            </td>


                                            <td class="text-center">
                                                <a href="{{ route('warehouse_employees.edit',[$user->id]) }}">
                                                    <span class="badge badge-pill badge-secondary btn btn-lg">تعديل</span>
                                                </a>
                                                <a onclick="deleteItem({{ $user->id }} , '{{ route('warehouse_employees.destroy',$user->id) }}')" >
                                                    <span class="badge badge-pill badge-danger btn btn-lg">حذف</span>
                                                </a>
                                            </td>


                                        </tr>


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop
@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
