@extends('Front.layouts.master')
@section('title', 'تعديل مقبوضات')
@section('styles')
<style>
    .product-td{
        width: 400px;
    }
</style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>رئيسية التحكم</h1>
            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">الرئيسية</a></li>
                    <li class="breadcrumb-item active" aria-current="page">تعديل  مقبوضات </li>
                </ol>
            </nav>
            <div class="separator mb-5"></div>
        </div>
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-body">
    <form action="{{ route('pay.users.update', ['id' => $payment->id]) }}" method="post">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        @include('Front.payments.form')
    </form>

            </div>
        </div>
        </div>
        </div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('.addRow').on('click',function(){
            alert('asdasd');
            addRow();
        });

        function addRow(){
            alert('test');
        }
    });
</script>
@endsection
