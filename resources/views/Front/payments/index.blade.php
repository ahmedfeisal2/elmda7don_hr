@extends('Front.layouts.master')

@section('title', 'Payments')
@section('styles')
<style>
    div.dataTables_filter {
        text-align: left;
        float: left;
    }
</style>
@endsection
@section('content')
    <div class="container-fluid disable-text-selection">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>مقبوضات</h1>
                    <div class="float-sm-right text-zero">

                           <a href="{{ route ('pay.users.create')}}"  class="btn btn-primary btn-lg mr-1">إضافة</a>

                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">المقبوضات</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">البيانات</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>

                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">

                    <div class="col-12 list">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title"> المقبوضات </h5>
                                <table id="example"  style="width:100%" class="dataTables table table-striped table-bordered cell-border compact stripe">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رقم العملية    </th>
                                        <th>إسم المستخدم   </th>
                                        <th>القمية قبل     </th>
                                        <th> القيمة بعد    </th>
                                        <th>  المدفوع       </th>
                                        <th> عدد الفواتير التابعه للعميل  </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments as $payment)
                                        <tr class="{{ $payment->id }}">
                                            <td> {{ $loop->iteration }} </td>
                                            <td>
                                                <p class="list-item-heading"> {{ $payment->id  }}</p>
                                            </td>
                                            <td>
                                                <p class="list-item-heading"> {{ $payment->user->name  }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ $payment->money_before }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$payment->money_after }} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$payment->money_paid}} </p>
                                            </td>
                                            <td>
                                                <p class="text-muted"> {{$payment->user->bills->count()  }}</p>
                                            </td>

                                            <td>
                                                {{--  <a href="{{ route('pay.users.edit',[$payment->id]) }}">
                                                    <span class="badge badge-pill badge-secondary">تعديل</span>
                                                </a>  --}}
                                                <a onclick="deleteItem({{ $payment->id }} , '{{ route('pay.users.delete',$payment->id) }}')" >
                                                    <span class="badge badge-pill badge-danger">حذف</span>
                                                </a>
                                            </td>


                                        </tr>

                                
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
  @include('Front.includes.delete_model')
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('<div class="loading">Loading</div>').appendTo('body');

        $('#example').DataTable({
            "initComplete": function( settings, json ) {
                $('div.loading').remove();
              },
            language:{
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            keys: true,
            scrollX:true,
            autoheight:true,



        });
        $('div.dataTables_filter input').focus()

    });
</script>
@endsection
