<!-- Basic modal -->
<div id="modal_default" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title"> هل آنت متآكد من عملية الحذف </h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="item" item-id="" route="">
                <p>هل آنت متآكد من عملية الحذف </p>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">تراجع </button>
                <button type="button" class="btn btn-danger" onclick="deleteModal();">تآكيد</button>
            </div>
        </div>
    </div>
</div>

<!-- /basic modal -->
