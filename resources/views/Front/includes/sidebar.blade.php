<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="active">
                    <a href="{{url('/')}}">
                        <i class="iconsmind-Shop-4"></i>
                        <span> الرئيسية </span>
                    </a>
                </li>

                <li class="">
                    <a href="#layouts">
                        <i class="iconsmind-Cash-register2"></i> الموردين
                    </a>
                </li>
                <li class="">
                    <a href="#layouts">
                        <i class="iconsmind-Digital-Drawing"></i> الموردين
                    </a>
                </li>
                <li>
                    <a href="#applications">
                        <i class="iconsmind-Air-Balloon"></i>  المنتجات
                    </a>
                </li>
                <li>
                    <a href="#ui">
                        <i class="iconsmind-Pantone"></i> المستخدمين
                    </a>
                </li>
                <li>
                    <a href="#landingPage">
                        <i class="iconsmind-Space-Needle"></i>  الفواتير
                    </a>
                </li>
                <li>
                    <a href="#menu">
                        <i class="iconsmind-Three-ArrowFork"></i> التقارير
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="dashboard">
                <li class="active">
                    <a href="Dashboard.Default.html">
                        <i class="simple-icon-rocket"></i> Default
                    </a>
                </li>
                <li>
                    <a href="Dashboard.Analytics.html">
                        <i class="simple-icon-pie-chart"></i>Analytics
                    </a>
                </li>
                <li>
                    <a href="Dashboard.Ecommerce.html">
                        <i class="simple-icon-basket-loaded"></i> Ecommerce
                    </a>
                </li>
                <li>
                    <a href="Dashboard.Content.html">
                        <i class="simple-icon-doc"></i> Content
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="layouts">
                <li>
                    <a href="Layouts.List.html">
                        <i class="simple-icon-credit-card"></i> Data List
                    </a>
                </li>
                <li>
                    <a href="Layouts.Thumbs.html">
                        <i class="simple-icon-list"></i> Thumb List
                    </a>
                </li>
                <li>
                    <a href="Layouts.Images.html">
                        <i class="simple-icon-grid"></i> Image List
                    </a>
                </li>
                <li>
                    <a href="Layouts.Details.html">
                        <i class="simple-icon-book-open"></i> Details
                    </a>
                </li>
                <li>
                    <a href="Layouts.Search.html">
                        <i class="simple-icon-magnifier"></i> Search
                    </a>
                </li>
                <li>
                    <a href="Layouts.Login.html">
                        <i class="simple-icon-user-following"></i> Login
                    </a>
                </li>
                <li>
                    <a href="Layouts.Register.html">
                        <i class="simple-icon-user-follow"></i> Register
                    </a>
                </li>
                <li>
                    <a href="Layouts.ForgotPassword.html">
                        <i class="simple-icon-user-unfollow"></i> Forgot Password
                    </a>
                </li>
                <li>
                    <a href="Layouts.Error.html">
                        <i class="simple-icon-exclamation"></i> Error
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="applications">
                <li>
                    <a href="Apps.MediaLibrary.html">
                        <i class="simple-icon-picture"></i> Library <span class="badge badge-pill badge-outline-primary float-right mr-4">NEW</span>
                    </a>
                </li>
                <li>
                    <a href="Apps.Todo.List.html">
                        <i class="simple-icon-check"></i> Todo
                    </a>
                </li>
                <li>
                    <a href="Apps.Survey.List.html">
                        <i class="simple-icon-calculator"></i> Survey
                    </a>
                </li>
                <li>
                    <a href="Apps.Chat.html">
                        <i class="simple-icon-bubbles"></i> Chat
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="ui">
                <li><a href="{{ route('users.index') }}"><i class="simple-icon-bell"></i> عرض الكل</a></li>
                <li><a href="{{route('users.create')}}"><i class="simple-icon-badge"></i> إضافة</a></li>
            </ul>

            <ul class="list-unstyled" data-link="landingPage">
                <li>
                    <a target="_blank" href="LandingPage.Home.html">
                        <i class="simple-icon-docs"></i> Multipage Home
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Home.Single.html">
                        <i class="simple-icon-doc"></i> Singlepage Home
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.About.html">
                        <i class="simple-icon-info"></i> About
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Auth.Login.html">
                        <i class="simple-icon-user-following"></i> Auth Login
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Auth.Register.html">
                        <i class="simple-icon-user-follow"></i> Auth Register
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Blog.html">
                        <i class="simple-icon-bubbles"></i> Blog
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Blog.Video.html">
                        <i class="simple-icon-bubble"></i> Blog Detail
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Careers.html">
                        <i class="simple-icon-people"></i> Careers
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Confirmation.html">
                        <i class="simple-icon-check"></i> Confirmation
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Contact.html">
                        <i class="simple-icon-phone"></i> Contact
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Content.html">
                        <i class="simple-icon-book-open"></i> Content
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Docs.html">
                        <i class="simple-icon-notebook"></i> Docs
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Features.html">
                        <i class="simple-icon-chemistry"></i> Features
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Prices.html">
                        <i class="simple-icon-wallet"></i> Prices
                    </a>
                </li>
                <li>
                    <a target="_blank" href="LandingPage.Videos.html">
                        <i class="simple-icon-film"></i> Videos
                    </a>
                </li>
            </ul>

            <ul class="list-unstyled" data-link="menu">
                <li>
                    <a href="Menu.Default.html">
                        <i class="simple-icon-control-pause"></i> Default
                    </a>
                </li>
                <li>
                    <a href="Menu.Subhidden.html">
                        <i class="simple-icon-arrow-left mi-subhidden"></i> Subhidden
                    </a>
                </li>
                <li>
                    <a href="Menu.Hidden.html">
                        <i class="simple-icon-control-start mi-hidden"></i> Hidden
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
