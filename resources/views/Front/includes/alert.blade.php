@if (session()->has("true"))

    <script>
        $.notify({
            // title:"<b>تم</b>",
            message:"{!! session()->get("true") !!}",
        },{
            // notification type
            type:"success"
        });

    </script>
@endif


@if (session()->has("info"))
    <script>

        $.notify({
            // title:"<b>توضيح</b>",
            message:"{{ session()->get("info")}}",
        },{
            // notification type
            type:"info"
        });

    </script>

@endif
@if(count($errors) > 0)
    <script>
        @foreach($errors->all() as $error)
        $.notify({
        title:"<b>رسالة خطآ</b>",
        message:"{{ $error }}",
        },{
            // notification type
            type:"danger"
        });


        @endforeach
    </script>

@endif
@if (session()->has("false"))
    <script>
        $.notify({
            title:"<b> تنبيه</b>",
            message:"{{session()->get("false") }}",
        },{
            // notification type
            type:"danger"
        });


    </script>

@endif
