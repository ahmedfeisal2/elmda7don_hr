@extends('Front.layouts.master')

@section('title', 'رئسية التحكم')

@section('styles')
<style>
    a.card{
        height: 200px;
    }

    a.card  .card-body{
       padding:  1.75rem;
    }
    a.card .card-body i {
        font-size: 2.7rem;
        margin-bottom: 1rem;
    }



</style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>رئيسية التحكم</h1>
            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                    <li class="breadcrumb-item active" aria-current="page">الرئيسية</li>
                </ol>
            </nav>
            <div class="separator mb-5"></div>
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12">
           <div class="row">


                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Alarm"></i>
                                <p class="lead card-text ">آقسام المنتجات</p>
                                <p class="lead text-center"> {{ \App\Models\Category::count()}} </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <div class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Male mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white">{{ \App\Models\Order::count() }} الطلبات</p>
                                        <p class="text-small text-white"></p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class="progress-bar-circle progress-bar-banner position-relative"
                                         data-color="white" data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="{{ \App\Models\Order::count() }}"
                                         aria-valuemax="1000" data-show-percent="false">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Basket-Coins"></i>
                                <p class="lead"> الآصناف </p>
                                <p class="lead text-center">{{ \App\Models\Product::count()}}</p>
                            </div>
                        </a>
                    </div>


                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Arrow-Refresh"></i>
                                <p class="lead"> العملاء  </p>
                                <p class="lead text-center">{{ \App\Models\User::where('type','user')->count() }}</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-4">
                        <div class="card mb-4 progress-banner">
                            <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                                <div>
                                    <i class="iconsmind-Bell mr-2 text-white align-text-bottom d-inline-block"></i>
                                    <div>
                                        <p class="lead text-white">{{ \App\Models\Safe::where('type','incomm')->count() }}  الإيداعات</p>
                                    </div>
                                </div>
                                <div>
                                    <div role="progressbar" class="progress-bar-circle progress-bar-banner position-relative"
                                         data-color="white" data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="{{ \App\Models\Safe::where('type','incomm')->count() }}"
                                         aria-valuemax="1000" data-show-percent="false">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4" style="display:inline-block">
                        <a href="#" class="card">
                            <div class="card-body text-center">
                                <i class="iconsmind-Mail-Read"></i>
                                <p class="lead ">الموردين </p>
                                <p class="lead text-center">{{ \App\Models\User::where('type','provider')->count() }}</p>
                            </div>
                        </a>
                    </div>
           </div>


                <div class="row">
                    <div class="col-md-12 mb-4 mt-4">
                        <div class="card">
                            <div class="position-absolute card-top-buttons">

                                <button class="btn btn-header-light icon-button" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="simple-icon-refresh"></i>
                                </button>

                                <div class="dropdown-menu dropdown-menu-right mt-3">
                                    <a class="dropdown-item" href="#">Sales</a>
                                    <a class="dropdown-item" href="#">Orders</a>
                                    <a class="dropdown-item" href="#">Refunds</a>
                                </div>

                            </div>

                            <div class="card-body">
                                <h5 class="card-title">الطلبات</h5>
                                <div class="dashboard-line-chart">
                                    <canvas id="salesChart1"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>


    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12  mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">حركة الخزنه</h5>
                    <div class="dashboard-donut-chart">
                        <canvas id="polarChart1"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xl-6 col-lg-12 mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">نتائج البحث</h5>
                    <table class="data-table responsive nowrap" data-order="[[ 1, &quot;desc&quot; ]]">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Sales</th>
                            <th>Stock</th>
                            <th>Category</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <p class="list-item-heading">Marble Cake</p>
                            </td>
                            <td>
                                <p class="text-muted">1452</p>
                            </td>
                            <td>
                                <p class="text-muted">62</p>
                            </td>
                            <td>
                                <p class="text-muted">Cupcakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Fruitcake</p>
                            </td>
                            <td>
                                <p class="text-muted">1245</p>
                            </td>
                            <td>
                                <p class="text-muted">65</p>
                            </td>
                            <td>
                                <p class="text-muted">Desserts</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Chocolate Cake</p>
                            </td>
                            <td>
                                <p class="text-muted">1200</p>
                            </td>
                            <td>
                                <p class="text-muted">45</p>
                            </td>
                            <td>
                                <p class="text-muted">Desserts</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Bebinca</p>
                            </td>
                            <td>
                                <p class="text-muted">1150</p>
                            </td>
                            <td>
                                <p class="text-muted">4</p>
                            </td>
                            <td>
                                <p class="text-muted">Cupcakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Napoleonshat</p>
                            </td>
                            <td>
                                <p class="text-muted">1050</p>
                            </td>
                            <td>
                                <p class="text-muted">41</p>
                            </td>
                            <td>
                                <p class="text-muted">Cakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Magdalena</p>
                            </td>
                            <td>
                                <p class="text-muted">998</p>
                            </td>
                            <td>
                                <p class="text-muted">24</p>
                            </td>
                            <td>
                                <p class="text-muted">Cakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Salzburger Nockerl</p>
                            </td>
                            <td>
                                <p class="text-muted">924</p>
                            </td>
                            <td>
                                <p class="text-muted">20</p>
                            </td>
                            <td>
                                <p class="text-muted">Desserts</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Soufflé</p>
                            </td>
                            <td>
                                <p class="text-muted">905</p>
                            </td>
                            <td>
                                <p class="text-muted">64</p>
                            </td>
                            <td>
                                <p class="text-muted">Cupcakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Cremeschnitte</p>
                            </td>
                            <td>
                                <p class="text-muted">845</p>
                            </td>
                            <td>
                                <p class="text-muted">12</p>
                            </td>
                            <td>
                                <p class="text-muted">Desserts</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Cheesecake</p>
                            </td>
                            <td>
                                <p class="text-muted">830</p>
                            </td>
                            <td>
                                <p class="text-muted">36</p>
                            </td>
                            <td>
                                <p class="text-muted">Desserts</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Gingerbread</p>
                            </td>
                            <td>
                                <p class="text-muted">807</p>
                            </td>
                            <td>
                                <p class="text-muted">21</p>
                            </td>
                            <td>
                                <p class="text-muted">Cupcakes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="list-item-heading">Goose Breast</p>
                            </td>
                            <td>
                                <p class="text-muted">795</p>
                            </td>
                            <td>
                                <p class="text-muted">9</p>
                            </td>
                            <td>
                                <p class="text-muted">Cupcakes</p>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-6 col-lg-6 col-sm-12 mb-4">
            <div class="card dashboard-search">
                <div class="card-body">
                    <h5 class="text-white mb-3">بحث متقدم</h5>
                    <div class="form-container">
                        <form action="#" method="GET" >
                            <div class="form-group">
                                <label>نوع البحث</label>
                                <select class="form-control select2-single" name="type" required>
                                    <option label="&nbsp;">&nbsp;</option>
                                    <option value="product"> منتج</option>
                                    <option value="provider">مورد</option>
                                    <option value="user">  مستخدم</option>
                                    <option value="bill">  فاتورة</option>
                                    <option value="save">  إيداع</option>
                                </select>
                            </div>
                            <div class="form-group" id="product_category">
                                <label>قسم المنتج</label>
                                <select class="form-control select2-single">
                                    <option label="&nbsp;">&nbsp;</option>
                                    <option value="TPZ"> </option>
                                    <option value="TTZ"></option>
                                </select>
                            </div>
                            <div class="form-group mb-5" id="last_date">
                                <label>تاريخ آخر تعامل</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control">
                                    <span class="input-group-text input-group-append input-group-addon">
                                            <i class="simple-icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group mb-5" id="bill_date">
                                <label>تاريخ  الفاتورة</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control">
                                    <span class="input-group-text input-group-append input-group-addon">
                                            <i class="simple-icon-calendar"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group mb-5" id="bill_save_money">
                                <label>  معدل المبلغ</label>
                                <div>
                                    <div class="slider" id="dashboardPriceRange"></div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary mt-3 pl-5 pr-5 ">بحث</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('js')
    <script src="{{ asset('js/dore.script.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script>

        $(document).ready(function(){
            $('#bill_save_money').hide();
            $('#bill_date').hide();
            $('#product_category').hide();
            $('#last_date').hide();
            var foregroundColor = '#145388';
            var primaryColor = '#212121';
            var separatorColor = '#d7d7d7';
            var  themeColor1 = '#145388';
            var  themeColor2 = '#2a93d5';
            var  themeColor3  ='#6c90a1';

            var themeColor1_10 = 'rgba(20, 83, 136, 0.1)';
            var themeColor2_10 = 'rgba(42, 147, 213, 0.1)';
            var themeColor3_10 = 'rgba(108, 144, 161, 0.1)';
            var order1 = {{ $order1 }}
            var order2 = {{ $order2 }}
            var order3 = {{ $order3 }}
            var order4 = {{ $order4 }}
            var order5 = {{ $order5 }}
            var order6 = {{ $order6 }}
            var order7 = {{ $order7 }}
            var incomm_safe = {{ $incomm_safe }}
            var outcomm_safe = {{ $outcomm_safe }}
            var chartTooltip = {
                backgroundColor: foregroundColor,
                titleFontColor: primaryColor,
                borderColor: separatorColor,
                borderWidth: 0.5,
                bodyFontColor: primaryColor,
                bodySpacing: 10,
                xPadding: 15,
                yPadding: 15,
                cornerRadius: 0.15,
                displayColors: false
            };
            if (typeof Chart !== "undefined") {

                Chart.defaults.global.defaultFontFamily = "'Nunito', sans-serif";

                Chart.defaults.LineWithShadow = Chart.defaults.line;
                Chart.controllers.LineWithShadow = Chart.controllers.line.extend({
                draw: function(ease) {
                    Chart.controllers.line.prototype.draw.call(this, ease);
                    var ctx = this.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.15)";
                    ctx.shadowBlur = 10;
                    ctx.shadowOffsetX = 0;
                    ctx.shadowOffsetY = 10;
                    ctx.responsive = true;
                    ctx.stroke();
                    Chart.controllers.line.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.BarWithShadow = Chart.defaults.bar;
                Chart.controllers.BarWithShadow = Chart.controllers.bar.extend({
                draw: function(ease) {
                    Chart.controllers.bar.prototype.draw.call(this, ease);
                    var ctx = this.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.15)";
                    ctx.shadowBlur = 12;
                    ctx.shadowOffsetX = 5;
                    ctx.shadowOffsetY = 10;
                    ctx.responsive = true;
                    Chart.controllers.bar.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.LineWithLine = Chart.defaults.line;
                Chart.controllers.LineWithLine = Chart.controllers.line.extend({
                draw: function(ease) {
                    Chart.controllers.line.prototype.draw.call(this, ease);

                    if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
                    var activePoint = this.chart.tooltip._active[0];
                    var ctx = this.chart.ctx;
                    var x = activePoint.tooltipPosition().x;
                    var topY = this.chart.scales["y-axis-0"].top;
                    var bottomY = this.chart.scales["y-axis-0"].bottom;

                    ctx.save();
                    ctx.beginPath();
                    ctx.moveTo(x, topY);
                    ctx.lineTo(x, bottomY);
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = "rgba(0,0,0,0.1)";
                    ctx.stroke();
                    ctx.restore();
                    }
                }
                });

                Chart.defaults.DoughnutWithShadow = Chart.defaults.doughnut;
                Chart.controllers.DoughnutWithShadow = Chart.controllers.doughnut.extend({
                draw: function(ease) {
                    Chart.controllers.doughnut.prototype.draw.call(this, ease);
                    let ctx = this.chart.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.15)";
                    ctx.shadowBlur = 10;
                    ctx.shadowOffsetX = 0;
                    ctx.shadowOffsetY = 10;
                    ctx.responsive = true;
                    Chart.controllers.doughnut.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.PieWithShadow = Chart.defaults.pie;
                Chart.controllers.PieWithShadow = Chart.controllers.pie.extend({
                draw: function(ease) {
                    Chart.controllers.pie.prototype.draw.call(this, ease);
                    let ctx = this.chart.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.15)";
                    ctx.shadowBlur = 10;
                    ctx.shadowOffsetX = 0;
                    ctx.shadowOffsetY = 10;
                    ctx.responsive = true;
                    Chart.controllers.pie.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.ScatterWithShadow = Chart.defaults.scatter;
                Chart.controllers.ScatterWithShadow = Chart.controllers.scatter.extend({
                draw: function(ease) {
                    Chart.controllers.scatter.prototype.draw.call(this, ease);
                    let ctx = this.chart.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.2)";
                    ctx.shadowBlur = 7;
                    ctx.shadowOffsetX = 0;
                    ctx.shadowOffsetY = 7;
                    ctx.responsive = true;
                    Chart.controllers.scatter.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.RadarWithShadow = Chart.defaults.radar;
                Chart.controllers.RadarWithShadow = Chart.controllers.radar.extend({
                draw: function(ease) {
                    Chart.controllers.radar.prototype.draw.call(this, ease);
                    let ctx = this.chart.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.2)";
                    ctx.shadowBlur = 7;
                    ctx.shadowOffsetX = 0;
                    ctx.shadowOffsetY = 7;
                    ctx.responsive = true;
                    Chart.controllers.radar.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                Chart.defaults.PolarWithShadow = Chart.defaults.polarArea;
                Chart.controllers.PolarWithShadow = Chart.controllers.polarArea.extend({
                draw: function(ease) {
                    Chart.controllers.polarArea.prototype.draw.call(this, ease);
                    let ctx = this.chart.chart.ctx;
                    ctx.save();
                    ctx.shadowColor = "rgba(0,0,0,0.2)";
                    ctx.shadowBlur = 10;
                    ctx.shadowOffsetX = 5;
                    ctx.shadowOffsetY = 10;
                    ctx.responsive = true;
                    Chart.controllers.polarArea.prototype.draw.apply(this, arguments);
                    ctx.restore();
                }
                });

                var chartTooltip = {
                backgroundColor: foregroundColor,
                titleFontColor: primaryColor,
                borderColor: separatorColor,
                borderWidth: 0.5,
                bodyFontColor: primaryColor,
                bodySpacing: 10,
                xPadding: 15,
                yPadding: 15,
                cornerRadius: 0.15,
                displayColors: false
                };

                var salesChart = document.getElementById("salesChart1").getContext("2d");

                var myChart = new Chart(salesChart, {
                    type: "LineWithShadow",
                    options: {
                      plugins: {
                        datalabels: {
                          display: false
                        }
                      },
                      responsive: true,
                      maintainAspectRatio: false,
                      scales: {
                        yAxes: [
                          {
                            gridLines: {
                              display: true,
                              lineWidth: 1,
                              color: "rgba(0,0,0,0.1)",
                              drawBorder: false
                            },
                            ticks: {
                              beginAtZero: true,
                              stepSize: 1,
                              min: 0,
                              max: 10,
                              padding: 10
                            }
                          }
                        ],
                        xAxes: [
                          {
                            gridLines: {
                              display: false
                            }
                          }
                        ]
                      },
                      legend: {
                        display: false
                      },
                      tooltips: chartTooltip
                    },
                    data: {
                      labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                      datasets: [
                        {
                          label: "",
                          data: [order1, order2, order3, order4, order5, order6, order7],
                          borderColor: themeColor1,
                          pointBackgroundColor: foregroundColor,
                          pointBorderColor: themeColor1,
                          pointHoverBackgroundColor: themeColor1,
                          pointHoverBorderColor: foregroundColor,
                          pointRadius: 6,
                          pointBorderWidth: 2,
                          pointHoverRadius: 8,
                          fill: false
                        }
                      ]
                    }
                });

                var polarChart = document.getElementById("polarChart1").getContext("2d");
                var myChart = new Chart(polarChart, {
                    type: "PolarWithShadow",
                    options: {
                    plugins: {
                        datalabels: {
                        display: false
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scale: {
                        ticks: {
                        display: false
                        }
                    },
                    legend: {
                        position: "bottom",
                        labels: {
                        padding: 30,
                        usePointStyle: true,
                        fontSize: 12
                        }
                    },
                    tooltips: chartTooltip
                    },
                    data: {
                    datasets: [
                        {
                        label: "Stock",
                        borderWidth: 2,
                        pointBackgroundColor: themeColor1,
                        borderColor: [themeColor1, themeColor2, themeColor3],
                        backgroundColor: [
                            themeColor1_10,
                            themeColor2_10,
                            themeColor3_10
                        ],
                        data: [incomm_safe, outcomm_safe]
                        }
                    ],
                    labels: ["الإيدارت", "آوذنات الصرف"]
                    }
                });

          }
        });

    </script>
@stop
